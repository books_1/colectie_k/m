# M

## Content

```
./M. A. Strenc:
M. A. Strenc - Taramul zeilor 0.9 '{SF}.docx

./M. Anjelais:
M. Anjelais - Fluturi striviti 1.0 '{Literatura}.docx

./M. C. Delasabar:
M. C. Delasabar - Iovita Valahul V1 1.0 '{ClubulTemerarilor}.docx
M. C. Delasabar - Iovita Valahul V2 1.0 '{ClubulTemerarilor}.docx

./M. C. Tudose:
M. C. Tudose - Eu sunt femeie 0.99 '{Dragoste}.docx

./M. Cleveley:
M. Cleveley - Alo politia 1.0 '{Detectiv}.docx
M. Cleveley - Teroare 1.0 '{Detectiv}.docx

./M. E. Saltikov Scedrin:
M. E. Saltikov Scedrin - Domnii Golovliov 1.0 '{Literatura}.docx

./M. J. Arlidge:
M. J. Arlidge - Helen Grace - V1 Ghici cine moare primul 1.0 '{Thriller}.docx
M. J. Arlidge - Helen Grace - V2 Ghici ce-i in cutie 1.0 '{Thriller}.docx
M. J. Arlidge - Helen Grace - V3 Casa papusilor 1.0 '{Thriller}.docx
M. J. Arlidge - Helen Grace - V4 Ghici care-i mincinosul 1.0 '{Thriller}.docx
M. J. Arlidge - Helen Grace - V5 Baiatul pierdut 1.0 '{Thriller}.docx
M. J. Arlidge - Helen Grace - V6 De-a v-ati ascunselea 1.0 '{Thriller}.docx

./M. John Harrison:
M. John Harrison - Lumina 1.0 '{SF}.docx
M. John Harrison - Turism 0.7 '{SF}.docx
M. John Harrison - Viriconium 1.0 '{SF}.docx

./M. Larrouy:
M. Larrouy - Uragan de sange si foc 0.9 '{Tineret}.docx

./M. R. Carey:
M. R. Carey - Fata cu toate darurile 1.0 '{Supranatural}.docx
M. R. Carey - Fellside 1.0 '{Supranatural}.docx

./M. Scott Peck:
M. Scott Peck - Drumul catre tine insuti si mai departe 1.0 '{Psihologie}.docx
M. Scott Peck - Psihologia mincunii 1.0 '{Psihologie}.docx

./M. Steiga & L.volfs:
M. Steiga & L.volfs - Garda la regina briliantelor 1.0 '{Politista}.docx

./Mabel Cole:
Mabel Cole - Adevarata identitate 0.9 '{Dragoste}.docx

./Mabel Katz:
Mabel Katz - Cea mai usoara cale de a-ti trai viata 0.9 '{Spiritualitate}.docx

./Mack Reynolds:
Mack Reynolds - Afacere caineasca 0.99 '{SF}.docx

./Madach Imre:
Madach Imre - Tragedia omului 0.9 '{Teatru}.docx

./Madeleine Brent:
Madeleine Brent - Lacrima lui Buddha 1.0 '{AventuraIstorica}.docx

./Madeleine Thien:
Madeleine Thien - Certitudine 0.99 '{Literatura}.docx

./Madeline Ashby:
Madeline Ashby - Orasul companiei 1.0 '{SF}.docx

./Madeline Harper:
Madeline Harper - Cantec fara sfarsit 0.99 '{Dragoste}.docx
Madeline Harper - Casa lebedei 0.99 '{Dragoste}.docx
Madeline Harper - Cer intunecat de noiembrie 0.2 '{Dragoste}.docx
Madeline Harper - Colierul venetian 0.9 '{Dragoste}.docx
Madeline Harper - Drumul spre Zaphir 0.99 '{Dragoste}.docx
Madeline Harper - Dupa ploaie 09 0.9 '{Dragoste}.docx
Madeline Harper - Fauritorii de vise 0.9 '{Dragoste}.docx
Madeline Harper - La apusul soarelui 0.9 '{Dragoste}.docx

./Madeline Ker:
Madeline Ker - Doamna virtuoasa 1.0 '{Romance}.docx

./Madi:
Madi - Ikiru este 0.7 '{SF}.docx

./Madi Ciocan Baluta:
Madi Ciocan Baluta - Pasii care nu se uita 0.99 '{Literatura}.docx

./Maestrul Kuthumi prin Michelle Eloff:
Maestrul Kuthumi prin Michelle Eloff - Activati-va chakra 5 dimensionala a inimii cu maestrul Iisus 1.0 '{Spiritualitate}.docx

./Magali:
Magali - Confruntare cu adevarul 0.2 '{Romance}.docx
Magali - Dilema 0.99 '{Romance}.docx

./Magda Contino:
Magda Contino - Iubire neimpartasita 0.2 '{Versuri}.docx

./Magda Isanos:
Magda Isanos - Saruta-ma 0.7 '{Dragoste}.docx

./Maggie Mitchell:
Maggie Mitchell - A doua fata 1.0 '{Politista}.docx

./Maggie O'Farrell:
Maggie O'Farrell - Iubire la distanta 1.0 '{Romance}.docx

./Maggie Stiefvater:
Maggie Stiefvater - Fratia Corbilor 1.0 '{Supranatural}.docx
Maggie Stiefvater - Lupii din Mercy Falls - V1 Fior 1.0 '{Supranatural}.docx
Maggie Stiefvater - Lupii din Mercy Falls - V2 Sovaire 1.0 '{Supranatural}.docx

./Magnus Sverdrup:
Magnus Sverdrup - Pionieri in vestul salbatic 2.0 '{Western}.docx

./Mai Jia:
Mai Jia - Criptograful 1.0 '{Literatura}.docx

./Maitreyi Devi:
Maitreyi Devi - Dragostea nu moare 1.0 '{Diverse}.docx

./Maja Lunde:
Maja Lunde - Istoria albinelor 1.0 '{Literatura}.docx

./Maj Sjowall:
Maj Sjowall - Martin Beck - V1 Roseanna 1.0 '{Literatura}.docx
Maj Sjowall - Martin Beck - V2 Disparut fara urma 1.0 '{Literatura}.docx

./Malala Yousafzai & Patricia McCormick:
Malala Yousafzai & Patricia McCormick - Eu sunt Malala 0.99 '{Literatura}.docx

./Malin Giolito:
Malin Giolito - Nisipuri miscatoare 1.0 '{Literatura}.docx

./Malthus:
Malthus - Eseu asupra principiului populatiei 1.0 '{Politica}.docx

./Mana S. Cummins:
Mana S. Cummins - Aprinzatorul de felinare 0.99 '{Tineret}.docx

./Manda Scott:
Manda Scott - Craniul de cristal 1.0 '{AventuraIstorica}.docx
Manda Scott - Visul vulturului 0.99 '{AventuraIstorica}.docx

./Manole Neagoe:
Manole Neagoe - Testamentul 0.9 '{IstoricaRo}.docx

./Mantak Chia:
Mantak Chia - Secretele taoiste ale iubirii 0.6 '{Spiritualitate}.docx

./Manuel Puig:
Manuel Puig - Cel mai frumos tangou 1.0 '{Literatura}.docx

./Manuel Scorza:
Manuel Scorza - Cantecul lui Agapito Robles 1.0 '{Literatura}.docx

./Mara Fitzcharles:
Mara Fitzcharles - Barbatul cu doua fete 0.9 '{Romance}.docx

./Mara Volkers:
Mara Volkers - Mireasa magului 0.99 '{AventuraIstorica}.docx

./Marc Carmoletti:
Marc Carmoletti - Secretissimo 0.8 '{Teatru}.docx

./Marc Charuel:
Marc Charuel - Blestemat Razboi 1.0 '{Razboi}.docx

./Marc de Smedt:
Marc de Smedt - Tehnici de meditatie si practici ale trezirii spirituale 0.7 '{Spiritualitate}.docx

./Marcel Brion:
Marcel Brion - Machiaveli, geniu si destin 1.0 '{AventuraIstorica}.docx
Marcel Brion - Orasul de nisip 0.99 '{SF}.docx

./Marcel Gherman:
Marcel Gherman - Generatia Matrix 1.0 '{SF}.docx

./Marcel Jouhandeau:
Marcel Jouhandeau - Astaroth sau vizitatorul nocturn 0.6 '{Diverse}.docx

./Marcel Jullian:
Marcel Jullian - Batalia Angliei 1.0 '{Istorie}.docx

./Marcel Luca:
Marcel Luca - Atac cu Spitfire la Binderdaal 1.0 '{SF}.docx
Marcel Luca - Cand nu vor fi gandaci de Colorado 0.99 '{SF}.docx
Marcel Luca - Caroline Project 0.99 '{SF}.docx
Marcel Luca - Contact esuat 0.99 '{SF}.docx
Marcel Luca - Dresorul de Brygi 0.99 '{SF}.docx
Marcel Luca - Ella 0.99 '{SF}.docx
Marcel Luca - Eroarea 0.99 '{SF}.docx
Marcel Luca - Explozia clipei 0.99 '{SF}.docx
Marcel Luca - Ghicitorul in Tarot 0.99 '{SF}.docx
Marcel Luca - Imposibila revedere 0.99 '{SF}.docx
Marcel Luca - Lacul pasarii Wandoo 0.99 '{SF}.docx
Marcel Luca - Nelinistile colonelului Malureanu 0.99 '{SF}.docx
Marcel Luca - Numarul magic 0.99 '{SF}.docx
Marcel Luca - Pata alba 0.99 '{SF}.docx
Marcel Luca - Scurta cariera a lui Antonio Rosales 0.99 '{SF}.docx
Marcel Luca - Tactica si strategie 0.9 '{SF}.docx
Marcel Luca - Timpul este umbra noastra 1.0 '{CalatorieinTimp}.docx
Marcel Luca - Triptic rusesc 1.0 '{SF}.docx
Marcel Luca - Ultimul cuvant 0.99 '{SF}.docx
Marcel Luca - Vitamina pentru imaginatie - Patru pastile S.F. 0.99 '{SF}.docx

./Marcel Petrisor:
Marcel Petrisor - Crisan 1.0 '{IstoricaRo}.docx

./Marcel Proust:
Marcel Proust - In Cautarea Timpului Pierdut - V1 Swan 1.0 '{ClasicSt}.docx
Marcel Proust - In Cautarea Timpului Pierdut - V2 La umbra fetelor in floare 1.0 '{ClasicSt}.docx
Marcel Proust - In Cautarea Timpului Pierdut - V3 Guermantes 1.0 '{ClasicSt}.docx
Marcel Proust - In Cautarea Timpului Pierdut - V5 Captiva 1.0 '{ClasicSt}.docx
Marcel Proust - In Cautarea Timpului Pierdut - V6 Fugara 1.0 '{ClasicSt}.docx
Marcel Proust - In Cautarea Timpului Pierdut - V7 Timpul regasit 0.99 '{ClasicSt}.docx

./Marc Gimenez:
Marc Gimenez - Culoarea legii 0.99 '{Thriller}.docx

./Marchizul de Sade:
Marchizul de Sade - Cele 120 de zile ale Sodomei 0.9 '{ClasicSt}.docx
Marchizul de Sade - Crimele iubirii 0.99 '{ClasicSt}.docx
Marchizul de Sade - Decameronul frantuzesc 1.1 '{ClasicSt}.docx
Marchizul de Sade - Justine sau nenorocirile virtutii 0.7 '{ClasicSt}.docx

./Marcia Evanick:
Marcia Evanick - Dimineata perfecta 0.99 '{Dragoste}.docx

./Marcial Lafuente Estefania:
Marcial Lafuente Estefania - Faimosul serif 1.0 '{Western}.docx

./Marcia Muller:
Marcia Muller - Trofee si lucruri moarte 0.99 '{Literatura}.docx

./Marcie Brown:
Marcie Brown - Chip de inger 0.99 '{Dragoste}.docx

./Marc J. Trennery:
Marc J. Trennery - Dinamita pentru Tirpitz 1.0 '{ActiuneRazboi}.docx

./Marc Laidlaw:
Marc Laidlaw - Demonstratia 0.9 '{SF}.docx

./Marc Levy:
Marc Levy - Copiii libertatii 1.0 '{Aventura}.docx
Marc Levy - Ea & El 1.0 '{Aventura}.docx
Marc Levy - Hotul de umbre 1.0 '{Aventura}.docx
Marc Levy - In alta viata 1.0 '{Aventura}.docx
Marc Levy - O fata ca ea 1.0 '{Aventura}.docx
Marc Levy - Orizontul rasturnat 1.0 '{Aventura}.docx
Marc Levy - Prietenii mei, iubirile mele 1.0 '{Aventura}.docx
Marc Levy - Prima zi & prima noapte 1.0 '{Aventura}.docx
Marc Levy - Sapte zile pentru o eternitate 1.0 '{Aventura}.docx
Marc Levy - Si daca e Adevarat - V1 Si daca e adevarat 2.0 '{Aventura}.docx
Marc Levy - Si daca e Adevarat - V2 Te voi revedea 4.0 '{Aventura}.docx
Marc Levy - Toate acele lucruri 1.0 '{Aventura}.docx
Marc Levy - Ultima din clanul Stanfield 1.0 '{Literatura}.docx
Marc Levy - Unde esti 1.0 '{Aventura}.docx

./Marco Buticchi:
Marco Buticchi - Inelul regilor 1.6 '{Aventura}.docx
Marco Buticchi - Vantul demonilor 1.0 '{Aventura}.docx

./Marco Malvaldi:
Marco Malvaldi - Masura omului 1.0 '{Literatura}.docx

./Marco Missiroli:
Marco Missiroli - Fidelitate 1.0 '{Diverse}.docx

./Marco Nese:
Marco Nese - Caracatita V1,2 2.0 '{Suspans}.docx
Marco Nese - Caracatita V3,4 2.0 '{Suspans}.docx
Marco Nese - Caracatita V5 1.0 '{Suspans}.docx

./Marcus Aurelius:
Marcus Aurelius - Catre sine 1.0 '{Filozofie}.docx

./Marcus Sedgwick:
Marcus Sedgwick - Sabia magica 1.0 '{Vampiri}.docx

./Marcus Tullius Cicero:
Marcus Tullius Cicero - Despre prietenie 0.99 '{Filozofie}.docx

./Marcus Valerius Martialis:
Marcus Valerius Martialis - Epigrame 1.0 '{Epigrame}.docx

./Marcy Stewart:
Marcy Stewart - Noaptea sfanta 0.99 '{Dragoste}.docx

./Marek Krajewski:
Marek Krajewski - Eberhard Mock - V1 Moarte la Breslau 1.0 '{Politista}.docx

./Margaret Atwood:
Margaret Atwood - Asasinul orb 1.0 '{SF}.docx
Margaret Atwood - Femeia comestibila 1.0 '{SF}.docx
Margaret Atwood - Galaad 2195 1.0 '{Diverse}.docx
Margaret Atwood - Oryx si Crake 1.0 '{SF}.docx
Margaret Atwood - Povestirea cameristei 3.0 '{SF}.docx

./Margaret Dawson:
Margaret Dawson - Aventura in port 0.99 '{Dragoste}.docx

./Margaret Drabble:
Margaret Drabble - Drumul stralucitor 1.0 '{Literatura}.docx

./Margaret Maddocks:
Margaret Maddocks - La tarmul marii 0.99 '{Diverse}.docx

./Margaret Millar:
Margaret Millar - In fiecare zi e iarna 1.0 '{Thriller}.docx

./Margaret Mitchell:
Margaret Mitchell - Pe aripile vantului 1.0 '{Dragoste}.docx

./Margaret Rome:
Margaret Rome - Fecioara de la gradinita 0.99 '{Romance}.docx

./Margaret Stanley:
Margaret Stanley - Baiatul din vecini 0.99 '{Dragoste}.docx
Margaret Stanley - Taramul iluziilor 0.9 '{Dragoste}.docx

./Margaret Summerton:
Margaret Summerton - Tu esti norocul meu 0.99 '{Dragoste}.docx

./Margarita Liberakis:
Margarita Liberakis - Palariile de pai 1.0 '{Dragoste}.docx

./Marge Smith:
Marge Smith - Cantecul din inima mea 0.9 '{Dragoste}.docx
Marge Smith - Pentru dragostea ta 0.9 '{Dragoste}.docx

./Margie Brown:
Margie Brown - Atatea lacrimi pentru nimic 0.99 '{Dragoste}.docx
Margie Brown - Barbatul irezistibil 0.7 '{Romance}.docx
Margie Brown - Departe de lumea nebuna 0.9 '{Dragoste}.docx

./Margie Ford:
Margie Ford - Frumosul meu aventurier 0.99 '{Dragoste}.docx

./Marguerite Duras:
Marguerite Duras - Amanta engleza 0.99 '{Diverse}.docx
Marguerite Duras - Durerea 0.99 '{Diverse}.docx
Marguerite Duras - Maladia mortii 1.0 '{Diverse}.docx
Marguerite Duras - Ochi albastri, par negru 0.99 '{Diverse}.docx

./Marguerite Yourcenar:
Marguerite Yourcenar - Obolul visului 0.8 '{Diverse}.docx
Marguerite Yourcenar - Povestiri orientale 0.99 '{Diverse}.docx

./Maria Angels Anglada:
Maria Angels Anglada - Vioara de la Auschwitz 1.0 '{Literatura}.docx

./Maria Baciu:
Maria Baciu - Frumoasa din prapastie 1.0 '{Politista}.docx

./Maria Banus:
Maria Banus - Magie interzisa 1.0 '{Teatru}.docx

./Maria Blanaru:
Maria Blanaru - Milin 0.99 '{Diverse}.docx

./Maria Contea:
Maria Contea - Inoceranii 1.0 '{Literatura}.docx

./Maria Corelli:
Maria Corelli - Barabas 0.99 '{AventuraIstorica}.docx

./Maria Duenas:
Maria Duenas - Iubirile croitoresei 0.9 '{Dragoste}.docx

./Maria Foldes:
Maria Foldes - Ce scurta e vara 1.0 '{Teatru}.docx

./Maria Gold:
Maria Gold - Jurnalul meu de maritata 0.9 '{Diverse}.docx

./Maria Housden:
Maria Housden - Pantofiorii rosii 1.0 '{Literatura}.docx

./Maria Ioan Antonopol:
Maria Ioan Antonopol - Nu va lepadati de Hristos, nu va insemnati cu 666 1.0 '{Religie}.docx

./Maria Kuncewiczowa:
Maria Kuncewiczowa - Straina 1.0 '{Literatura}.docx

./Maria Mihaiescu:
Maria Mihaiescu - Dragoste neinteleasa 0.9 '{Diverse}.docx

./Maria Montessori:
Maria Montessori - Descoperirea copilului 1.0 '{DezvoltarePersonala}.docx
Maria Montessori - Mintea absorbanta 1.0 '{DezvoltarePersonala}.docx
Maria Montessori - Secretul copilariei 1.0 '{DezvoltarePersonala}.docx

./Mariana Gavrila:
Mariana Gavrila - Osiris din Carpati 0.9 '{Spiritualitate}.docx

./Marian Coman:
Marian Coman - Haiganu. fluviul soaptelor 1.0 '{SF}.docx
Marian Coman - Usa de la baie 0.99 '{SF}.docx

./Marian Dumitrascu:
Marian Dumitrascu - Bumerangul lui Zeeler 0.99 '{SF}.docx

./Marian Melinte:
Marian Melinte - Acuarela 0.9 '{SF}.docx

./Marianne Wurtz:
Marianne Wurtz - Clipe decisive 0.99 '{Dragoste}.docx

./Mariano Azuela:
Mariano Azuela - Cei din vale 1.0 '{Literatura}.docx

./Marian Popa:
Marian Popa - Calatorie sprancenata 1.0 '{Literatura}.docx

./Marian Truta:
Marian Truta - De ce cad ingerii 0.8 '{Diverse}.docx

./Maria Pongracz:
Maria Pongracz - Tablouri dintr-o expozitie 0.9 '{Diverse}.docx

./Maria Popescu Butucea:
Maria Popescu Butucea - Octopodul 0.99 '{SF}.docx

./Maria Semple:
Maria Semple - Unde ai disparut, Bernadette 0.9 '{Literatura}.docx

./Maria Szepes:
Maria Szepes - Leul rosu 1.0 '{Spiritualitate}.docx

./Maria Todorova:
Maria Todorova - Balcanii si balcanismul 1.0 '{Istorie}.docx

./Marie Anne Desmarest:
Marie Anne Desmarest - Torente 2.0 '{Dragoste}.docx
Marie Anne Desmarest - Torente V1 3.0 '{Dragoste}.docx
Marie Anne Desmarest - Torente V2 3.0 '{Dragoste}.docx
Marie Anne Desmarest - Torente V3 3.0 '{Dragoste}.docx
Marie Anne Desmarest - Torente V4 3.0 '{Dragoste}.docx
Marie Anne Desmarest - Torente V5 3.0 '{Dragoste}.docx
Marie Anne Desmarest - Torente V6 3.0 '{Dragoste}.docx

./Marie Bourdon:
Marie Bourdon - Copie perfecta 0.99 '{Dragoste}.docx

./Marie Chaix:
Marie Chaix - Tacerile sau viata unei femei 1.0 '{Dragoste}.docx

./Marie Costes:
Marie Costes - Explozie pe Minerva 0.9 '{Romance}.docx

./Marie Defois:
Marie Defois - O sansa nesperata 0.9 '{Dragoste}.docx

./Marie Dutronc:
Marie Dutronc - Licitatia secolului 0.8 '{Dragoste}.docx

./Marie Francoise Colombani:
Marie Francoise Colombani - Millennium, Stieg si Eu 2.0 '{Thriller}.docx

./Marie Lu:
Marie Lu - Legend - V1 Legenda 1.0 '{SF}.docx
Marie Lu - Legend - V2 Geniul 1.0 '{SF}.docx
Marie Lu - Legend - V3 Campionul 1.0 '{SF}.docx

./Marie Michael:
Marie Michael - Din aur si lumina 0.9 '{Romance}.docx

./Marie Phillips:
Marie Phillips - Zeii pusi pe rele 1.0 '{Aventura}.docx

./Marie Renee Lavoie:
Marie Renee Lavoie - Autopsia unei asa-zise casnicii banale 1.0 '{Dragoste}.docx

./Marie Rutkoski:
Marie Rutkoski - Blestemul castigatorului 1.0 '{Literatura}.docx

./Marie Stanton:
Marie Stanton - Enigma de la Glenroden 0.99 '{Romance}.docx

./Marie Therese Cuny:
Marie Therese Cuny - Leila. Maritata cu forta 1.0 '{Diverse}.docx

./Marilyn Baker:
Marilyn Baker - Fiul pe care ti-l doreai 0.99 '{Dragoste}.docx

./Marilyn Herr:
Marilyn Herr - Fara minciuni intre noi 0.99 '{Dragoste}.docx

./Marilyn Jordan:
Marilyn Jordan - Hoinarul marilor 0.99 '{Dragoste}.docx
Marilyn Jordan - Strainul din trecut 0.99 '{Dragoste}.docx

./Marilyn Manning:
Marilyn Manning - Aventura dragostei 0.99 '{Dragoste}.docx

./Marilyn Mayo:
Marilyn Mayo - Traind din amintiri 0.99 '{Dragoste}.docx

./Marilynne Robinson:
Marilynne Robinson - Gilead - V1 Galaad 1.0 '{Literatura}.docx
Marilynne Robinson - Gilead - V2 Acasa 1.0 '{Literatura}.docx

./Marina Anderson:
Marina Anderson - Scoala supunerii 1.0 '{Erotic}.docx
Marina Anderson - V1 Secrete intunecate 1.0 '{Erotic}.docx
Marina Anderson - V2 Dorinte interzise 1.0 '{Erotic}.docx

./Marina Duenas:
Marina Duenas - Iubirile croitoresei 0.8 '{Literatura}.docx

./Marina Stepnova:
Marina Stepnova - Femeile lui Lazar 2.0 '{Literatura}.docx

./Marina Thomas:
Marina Thomas - Croaziera uitarii 0.99 '{Dragoste}.docx

./Marin Ionita:
Marin Ionita - Capul lui Decebal 1.0 '{IstoricaRo}.docx

./Marin Preda:
Marin Preda - Cel mai iubit dintre pamanteni 1.0 '{ClasicRo}.docx
Marin Preda - Creatie si morala 1.0 '{ClasicRo}.docx
Marin Preda - Delirul 1.0 '{ClasicRo}.docx
Marin Preda - Desfasurarea 0.9 '{ClasicRo}.docx
Marin Preda - Imposibila intoarcere 1.0 '{ClasicRo}.docx
Marin Preda - Intalnirea din pamanturi 1.0 '{ClasicRo}.docx
Marin Preda - Intrusul 1.0 '{ClasicRo}.docx
Marin Preda - Jurnal intim 1.0 '{ClasicRo}.docx
Marin Preda - Marele singuratic 1.0 '{ClasicRo}.docx
Marin Preda - Martin Bormann 1.0 '{Teatru}.docx
Marin Preda - Morometii V1 1.0 '{ClasicRo}.docx
Marin Preda - Morometii V2 1.0 '{ClasicRo}.docx
Marin Preda - Risipitorii 1.0 '{ClasicRo}.docx
Marin Preda - Scrieri de tinerete 1.0 '{ClasicRo}.docx
Marin Preda - Tineretea lui Moromete 1.0 '{ClasicRo}.docx
Marin Preda - Viata ca o prada 1.0 '{ClasicRo}.docx

./Marin Sorescu:
Marin Sorescu - Descantoteca 1.0 '{Versuri}.docx
Marin Sorescu - Iona 2.0 '{ClasicRo}.docx
Marin Sorescu - La lilieci V1 1.0 '{Versuri}.docx
Marin Sorescu - La lilieci V2 2.0 '{Versuri}.docx
Marin Sorescu - La lilieci V3 1.0 '{Versuri}.docx
Marin Sorescu - La lilieci V4 1.0 '{Versuri}.docx
Marin Sorescu - La lilieci V5 1.0 '{Versuri}.docx
Marin Sorescu - La lilieci V6 1.0 '{Versuri}.docx
Marin Sorescu - Poezii 0.9 '{Versuri}.docx
Marin Sorescu - Trei dinti din fata 1.0 '{ClasicRo}.docx
Marin Sorescu - Unde fugim de acasa 2.0 '{ClasicRo}.docx
Marin Sorescu - Unde fugim de acasa 2.0 (Poze) '{ClasicRo}.docx

./Mario Bellatin:
Mario Bellatin - Salon de frumusete 0.99 '{Literatura}.docx

./Marion Lennox:
Marion Lennox - Copii lui Tom Bradley 0.99 '{Dragoste}.docx

./Marion Zimmer Bradley:
Marion Zimmer Bradley - Avalon - V1 Negurile V1 2.0 '{SF}.docx
Marion Zimmer Bradley - Avalon - V1 Negurile V2 2.0 '{SF}.docx
Marion Zimmer Bradley - Avalon - V2 Sanctuarul 1.1 '{SF}.docx
Marion Zimmer Bradley - Avalon - V5 Doamna din Avalon 1.0 '{SF}.docx
Marion Zimmer Bradley - Avalon - V6 Preoteasa din Avalon 1.0 '{SF}.docx
Marion Zimmer Bradley - Avalon - V7 Negurile V1 1.0 '{SF}.docx
Marion Zimmer Bradley - Avalon - V7 Negurile V2 1.0 '{SF}.docx

./Mario Pissi:
Mario Pissi - Poarta catre cer 0.7 '{Spiritualitate}.docx

./Mario Puzo:
Mario Puzo - Arena sumbra 1.0 '{Thriller}.docx
Mario Puzo - Nasul 1.0 '{Thriller}.docx
Mario Puzo - Norocosul pelerin 1.0 '{Thriller}.docx
Mario Puzo - Omerta 1.0 '{Thriller}.docx
Mario Puzo - Ultimul Don 1.0 '{Thriller}.docx

./Mario Reading:
Mario Reading - Nostradamus. misterul catrenelor pierdute 0.9 '{Thriller}.docx

./Mario Soldati:
Mario Soldati - Cele doua orase 1.0 '{Dragoste}.docx

./Mario Vargas Llosa:
Mario Vargas Llosa - Baietii si alte povestiri 1.0 '{Literatura}.docx
Mario Vargas Llosa - Caietele lui Don Rigoberto 0.9 '{Literatura}.docx
Mario Vargas Llosa - Casa verde 1.0 '{Literatura}.docx
Mario Vargas Llosa - Cinci colturi 1.0 '{Literatura}.docx
Mario Vargas Llosa - Cine l-a ucis pe Palomino Molero 0.99 '{Literatura}.docx
Mario Vargas Llosa - Conversatie la catedrala 1.0 '{Literatura}.docx
Mario Vargas Llosa - Elogiu mamei vitrege 1.0 '{Erotic}.docx
Mario Vargas Llosa - Istoria lui Mayta 1.0 '{Literatura}.docx
Mario Vargas Llosa - Lituma in Anzi 1.0 '{Literatura}.docx
Mario Vargas Llosa - Matusa Julia si condeierul 1.0 '{Literatura}.docx
Mario Vargas Llosa - Orasul si cainii 1.0 '{Literatura}.docx
Mario Vargas Llosa - Pantaleon si vizitatoarele 1.0 '{Literatura}.docx
Mario Vargas Llosa - Paradisul de dupa colt 1.0 '{Literatura}.docx
Mario Vargas Llosa - Povestasul 0.99 '{Literatura}.docx
Mario Vargas Llosa - Ratacirile fetei nesabuite 1.0 '{Literatura}.docx
Mario Vargas Llosa - Razboiul sfarsitului lumii 1.0 '{Literatura}.docx
Mario Vargas Llosa - Sarbatoarea tapului 1.0 '{Literatura}.docx
Mario Vargas Llosa - Scrisori catre un tanar romancier 0.9 '{Literatura}.docx
Mario Vargas Llosa - Tentatia imposibilului 0.7 '{Literatura}.docx

./Marisha Pessl:
Marisha Pessl - De veghe intre lumi 1.0 '{Literatura}.docx

./Marius Dobrescu:
Marius Dobrescu - Intalnirea de la plopul ars 1.0 '{Tineret}.docx

./Marius Ghilezan:
Marius Ghilezan - Impostura. Despre snobism si puterea falsului 0.7 '{Psihologie}.docx

./Marius Oprea:
Marius Oprea - Ascensiunea lui Traian Basescu 0.9 '{Politica}.docx
Marius Oprea - Chipul mortii. Dialog cu Vladimir Bukovski 0.9 '{Politica}.docx

./Marius Theodor Barna:
Marius Theodor Barna - Fata in fata 0.9 '{Teatru}.docx

./Marjorie Buckingham:
Marjorie Buckingham - Doroteea 0.8 '{Diverse}.docx

./Marjorie Norrell:
Marjorie Norrell - Mireasa pentru o zi 0.99 '{Dragoste}.docx

./Marjorie Warby:
Marjorie Warby - Fata din gradina 0.99 '{Dragoste}.docx
Marjorie Warby - Medicul celibatar 0.99 '{Dragoste}.docx
Marjorie Warby - Sa inceapa spectacolul 0.99 '{Romance}.docx
Marjorie Warby - Surorile Gregory 0.99 '{Romance}.docx

./Mark Allen Smith:
Mark Allen Smith - Inchizitorul 1.0 '{Thriller}.docx

./Mark Clifton & Frank Riley:
Mark Clifton & Frank Riley - Masina eternitatii 0.9 '{CalatorieinTimp}.docx

./Mark Forsyth:
Mark Forsyth - Scurta istorie a betiei 1.0 '{Istorie}.docx

./Mark Frost:
Mark Frost - Cei 6 Mesia 1.0 '{Politista}.docx
Mark Frost - Lista celor 7 1.0 '{Politista}.docx

./Mark Haddon:
Mark Haddon - O intamplare ciudata cu un caine la miezul noptii 1.0 '{Literatura}.docx
Mark Haddon - Pata cu bucluc 0.9 '{Literatura}.docx

./Mark Helprin:
Mark Helprin - Poveste de iarna 1.0 '{Literatura}.docx

./Mark Manson:
Mark Manson - Arta subtila a nepasarii 1.0 '{DezvoltarePersonala}.docx

./Mark Robson:
Mark Robson - Spioana imperiala 1.0 '{AventuraTineret}.docx

./Mark Sullivan:
Mark Sullivan - Sub un cer sangeriu 1.0 '{Literatura}.docx

./Mark Twain:
Mark Twain - Autobiografie 0.9 '{AventuraTineret}.docx
Mark Twain - Aventurile lui Huckleberry Finn 1.0 '{AventuraTineret}.docx
Mark Twain - Aventurile lui Tom Sawyer 2.0 '{AventuraTineret}.docx
Mark Twain - Bancnota de un milion de lire 1.0 '{AventuraTineret}.docx
Mark Twain - Jurnalul lui Adam si al Evei 1.0 '{AventuraTineret}.docx
Mark Twain - Opere V3 0.9 '{AventuraTineret}.docx
Mark Twain - Pretendentul american 1.0 '{AventuraTineret}.docx
Mark Twain - Print si cersetor 1.0 '{AventuraTineret}.docx
Mark Twain - Strainul misterios 1.0 '{AventuraTineret}.docx
Mark Twain - Tom Sawyer detectiv. Tom Sawyer in strainatate 1.0 '{AventuraTineret}.docx
Mark Twain - Tom Sawyer detectiv 1.0 '{AventuraTineret}.docx
Mark Twain - Tom Sawyer in strainatate 1.0 '{AventuraTineret}.docx
Mark Twain - Un yankeu la curtea regelui Arthur 1.0 '{AventuraTineret}.docx
Mark Twain - Vorbesc din mormant 1.0 '{Biografie}.docx

./Markus Zusak:
Markus Zusak - Hotul de carti 1.0 '{Thriller}.docx
Markus Zusak - Mesagerul 1.0 '{Thriller}.docx
Markus Zusak - Podul de lut 1.0 '{Thriller}.docx

./Mark Vernon:
Mark Vernon - 42 de ganduri profunde 1.0 '{Filozofie}.docx

./Mark Winegardner:
Mark Winegardner - Intoarcerea nasului 1.0 '{Thriller}.docx
Mark Winegardner - Razbunarea nasului 0.99 '{Thriller}.docx

./Marnie de Marez:
Marnie de Marez - O sotie nedorita 0.99 '{Romance}.docx

./Marsha Clark:
Marsha Clark - Cuibul zeilor 0.8 '{Dragoste}.docx
Marsha Clark - Dincolo de munti 0.9 '{Dragoste}.docx
Marsha Clark - Focuri in Queensland 0.99 '{Dragoste}.docx
Marsha Clark - Obstacole depasite 0.2 '{Romance}.docx
Marsha Clark - Ramai dragostea mea 0.9 '{Romance}.docx

./Martha Hall Kelly:
Martha Hall Kelly - Cand infloreste liliacul 1.0 '{Razboi}.docx

./Martha Schroeder:
Martha Schroeder - De doua ori sfioasa 0.99 '{Dragoste}.docx

./Martha Vincent:
Martha Vincent - Valul amintirilor 0.9 '{Dragoste}.docx

./Martina Cole:
Martina Cole - Afacerea 1.0 '{Literatura}.docx
Martina Cole - Criminala din dragoste 1.0 '{Literatura}.docx
Martina Cole - Noapte buna, doamna 1.0 '{Literatura}.docx
Martina Cole - Sacrificiul 1.0 '{Literatura}.docx

./Martin Amis:
Martin Amis - Banii 0.9 '{Literatura}.docx
Martin Amis - Casa intalnirilor 1.0 '{Literatura}.docx
Martin Amis - Nascuti morti 1.0 '{Literatura}.docx

./Martin Andersen Nexo:
Martin Andersen Nexo - Ditte, fiica omului 1.0 '{Literatura}.docx

./Martin Andreu:
Martin Andreu - Omul cu briciul 2.0 '{Thriller}.docx

./Martin Brofman:
Martin Brofman - Oricine poate fi vindecat 0.9 '{Sanatate}.docx

./Martin Caparros:
Martin Caparros - Enigma Valfierno 0.99 '{Thriller}.docx

./Martin Colin:
Martin Colin - Bun venit in infern 0.9 '{Comunism}.docx

./Martin Gardner:
Martin Gardner - Profesorul fara nici o parte 0.99 '{SF}.docx

./Martin Hoffer:
Martin Hoffer - OZN reteaua canadiana 0.8 '{MistersiStiinta}.docx

./Martin Homann:
Martin Homann - Zbaterea numita viata 0.9 '{Religie}.docx

./Martin Langfield:
Martin Langfield - Cutia raului 1.0 '{Suspans}.docx

./Martin Millar:
Martin Millar - Zanele punkiste din New York 1.0 '{SF}.docx

./Martin Page:
Martin Page - M-am hotarat sa devin prost 0.9 '{Umor}.docx

./Martin Seay:
Martin Seay - Hotul de oglinzi 1.0 '{Literatura}.docx

./Martin Smith:
Martin Smith - Gorki Park 1.0 '{Thriller}.docx

./Martin Suter:
Martin Suter - Diavolul din Milano 1.0 '{Literatura}.docx
Martin Suter - Maestrul bucatar 1.0 '{Literatura}.docx
Martin Suter - Small World 1.0 '{Literatura}.docx
Martin Suter - Un prieten perfect 1.0 '{Literatura}.docx

./Martin Walser:
Martin Walser - Clocot 0.8 '{Literatura}.docx

./Marvin White:
Marvin White - Istoria interzisa a omenirii si conexiunea extraterestra 1.0 '{MistersiStiinta}.docx

./Mary Adler:
Mary Adler - Vacanta de vara 1.0 '{Romance}.docx
Mary Adler - Vis de vacanta 1.0 '{Romance}.docx

./Mary Anne Wilson:
Mary Anne Wilson - Undeva la San Diego 0.2 '{Dragoste}.docx

./Mary Ann Gibbs:
Mary Ann Gibbs - Cel mai romantic oras 0.9 '{Diverse}.docx

./Mary Ann Phillips:
Mary Ann Phillips - Iarna trandafirilor albi 0.9 '{Dragoste}.docx

./Mary Ann Shaffer:
Mary Ann Shaffer - Scrisori din insula Guernsey 1.0 '{Literatura}.docx

./Mary Arbor:
Mary Arbor - Alergand dupa un vis 1.0 '{Romance}.docx

./Mary Balogh:
Mary Balogh - Amantele - V1 Mai mult decat amanta 1.0 '{Romance}.docx
Mary Balogh - Amantele - V2 Nu sunt amanta nimanui 1.0 '{Romance}.docx
Mary Balogh - Pretul nesocotintei 1.0 '{Romance}.docx
Mary Balogh - Simply Quartet - V1 Pasiune si prejudecata 1.0 '{Romance}.docx
Mary Balogh - Simply Quartet - V2 Suflete pereche 1.0 '{Romance}.docx
Mary Balogh - Simply Quartet - V3 Iubire magica 1.0 '{Romance}.docx
Mary Balogh - Simply Quartet - V4 Dragostea invinge 1.0 '{Romance}.docx

./Mary Brabham:
Mary Brabham - Pasiune si razbunare 0.99 '{Dragoste}.docx

./Mary Catherine Hanson:
Mary Catherine Hanson - Castelul Wicklow 0.9 '{Romance}.docx
Mary Catherine Hanson - Portretul femeii iubite 0.99 '{Dragoste}.docx

./Mary Cooper:
Mary Cooper - Excursie in padure 0.9 '{Romance}.docx

./Mary Essex:
Mary Essex - Asistenta indragostita 0.99 '{Romance}.docx
Mary Essex - Poveste de dragoste la Tanger 0.99 '{Dragoste}.docx

./Mary Grace:
Mary Grace - Intoarcerea eroului 0.99 '{Dragoste}.docx

./Mary Higgins Clark:
Mary Higgins Clark - Casa blestemata 2.0 '{Thriller}.docx
Mary Higgins Clark - Confesionalul 0.99 '{Thriller}.docx
Mary Higgins Clark - Escrocheria 0.99 '{Thriller}.docx
Mary Higgins Clark - Lasa-ma sa-ti spun iubita mea 1.0 '{Thriller}.docx
Mary Higgins Clark - Pactul cu diavolul 0.99 '{Thriller}.docx
Mary Higgins Clark - Prefa-te ca n-ai observat 1.0 '{Thriller}.docx
Mary Higgins Clark - Strainul care sta la panda 0.99 '{Thriller}.docx

./Mary Irwin:
Mary Irwin - In cautarea mamei 0.9 '{Dragoste}.docx

./Mary James:
Mary James - Designer pentru moarte 0.9 '{Dragoste}.docx
Mary James - Dragostea invinge mandria 0.9 '{Dragoste}.docx
Mary James - Regrete chinuitoare 0.8 '{Dragoste}.docx

./Mary Jo Putney:
Mary Jo Putney - Compromisul inimii 1.0 '{Dragoste}.docx
Mary Jo Putney - Dansul pasiunii 1.0 '{Dragoste}.docx
Mary Jo Putney - Daruri periculoase 1.0 '{Dragoste}.docx
Mary Jo Putney - Destine Nobile - V1 O noua viata 1.0 '{Dragoste}.docx
Mary Jo Putney - Destine Nobile - V2 Umbrele trecutului 0.9 '{Dragoste}.docx
Mary Jo Putney - Destine Nobile - V3 Pasiune si onoare 1.0 '{Dragoste}.docx
Mary Jo Putney - Imblanzirea unui rebel 0.99 '{Dragoste}.docx
Mary Jo Putney - Raul de foc 1.0 '{Dragoste}.docx

./Mary Kubica:
Mary Kubica - Fata cea buna 1.0 '{Literatura}.docx
Mary Kubica - Necunoscuta 1.0 '{Thriller}.docx

./Marylee Anderson:
Marylee Anderson - De un milion de ori maine 1.0 '{Romance}.docx

./Mary Lou Rich:
Mary Lou Rich - Arca norei 0.9 '{Romance}.docx

./Mary Lynn Baxter:
Mary Lynn Baxter - Fulgere si trasnete V1 0.99 '{Dragoste}.docx
Mary Lynn Baxter - Fulgere si trasnete V2 0.99 '{Dragoste}.docx

./Mary Mcneal:
Mary Mcneal - Prizoniera seicului 0.99 '{Dragoste}.docx

./Mary Meredith:
Mary Meredith - Insula Tobei de Aur 0.9 '{Dragoste}.docx
Mary Meredith - Naufragiatii 0.9 '{Romance}.docx

./Mary Morgan:
Mary Morgan - Dupa ruperea logodnei 0.9 '{Dragoste}.docx
Mary Morgan - Evadata din harem 0.9 '{Dragoste}.docx
Mary Morgan - Intoarcere la privelistea diavolului 0.9 '{Dragoste}.docx
Mary Morgan - O viata periculoasa 0.9 '{Dragoste}.docx
Mary Morgan - Paradisul regasit 0.99 '{Dragoste}.docx
Mary Morgan - Visuri interzise 0.99 '{Dragoste}.docx

./Mary Pearson:
Mary Pearson - Intalnire cu razbunarea 0.9 '{Dragoste}.docx
Mary Pearson - Soare si umbra 0.99 '{Romance}.docx

./Mary Raymond:
Mary Raymond - Era oare noapte 0.9 '{Romance}.docx

./Mary Rosenblum:
Mary Rosenblum - Filme de amatori 1.0 '{SF}.docx

./Mary Scott:
Mary Scott - Da iubito 0.99 '{Dragoste}.docx
Mary Scott - Secretul chirurgului 0.9 '{Dragoste}.docx

./Mary Shelley:
Mary Shelley - Ultimul om 0.99 '{Literatura}.docx

./Mary W. Shelley:
Mary W. Shelley - Frankenstein sau Prometeul modern 1.0 '{Supranatural}.docx

./Matei Calinescu:
Matei Calinescu - Portretul lui M 0.9 '{Biografie}.docx

./Matei Florian:
Matei Florian - Si Hams si Regretel 0.9 '{Diverse}.docx

./Matei Gavril:
Matei Gavril - Noaptea definitiva 0.8 '{Diverse}.docx

./Mateiu Caragiale:
Mateiu Caragiale - Craii de curtea veche 1.0 '{Diverse}.docx
Mateiu Caragiale - Remember 1.0 '{Diverse}.docx
Mateiu Caragiale - Sub pecetea tainei 1.0 '{Diverse}.docx

./Matei Visniec:
Matei Visniec - Despre sexul femeii 1.0 '{Teatru}.docx
Matei Visniec - Frumoasa calatorie a ursilor Panda povestita de un saxofonist 0.99 '{Teatru}.docx
Matei Visniec - Inteleptul la ora de ceai 0.9 '{Versuri}.docx
Matei Visniec - Istoria comunismului povestita pentru bolnavii mintali 0.9 '{Teatru}.docx
Matei Visniec - Mansarda 1.0 '{Teatru}.docx
Matei Visniec - Paparazzi sau cronica unui rasarit de soare avortat 0.9 '{Teatru}.docx
Matei Visniec - Teatru scurt 1.0 '{Teatru}.docx

./Matila C. Ghyka:
Matila C. Ghyka - Filosofia si mistica numarului 1.0 '{Matematica}.docx

./Matt Beynon Rees:
Matt Beynon Rees - Crimele din Betleem 1.0 '{Thriller}.docx
Matt Beynon Rees - Secretul Samariteanului 1.0 '{Thriller}.docx
Matt Beynon Rees - Un mormant in Gaza 1.0 '{Thriller}.docx

./Matt Haig:
Matt Haig - Baiatul Echo 1.0 '{SF}.docx
Matt Haig - Cum sa opresti timpul 1.0 '{Tineret}.docx
Matt Haig - Eu si Mos Craciun 1.0 '{Tineret}.docx
Matt Haig - Fetita care a salvat Craciunul 1.0 '{Tineret}.docx
Matt Haig - Umanii 1.0 '{SF}.docx
Matt Haig - Un baiat numit Craciun 1.0 '{Tineret}.docx

./Matthew FitzSimmons:
Matthew FitzSimmons - Gibson Vaughn - V1 Disparitia 1.0 '{Diverse}.docx

./Matthew J. Kirby:
Matthew J. Kirby - New Assassin's Creed - V1 Ultimii descendenti 1.0 '{AventuraIstorica}.docx

./Matthew Reilly:
Matthew Reilly - Complex 13 1.0 '{SF}.docx
Matthew Reilly - Planul Majestic 12 2.0 '{Suspans}.docx
Matthew Reilly - Statia polara 2.0 '{Suspans}.docx
Matthew Reilly - Templul 2.0 '{Suspans}.docx
Matthew Reilly - Zona 7 2.0 '{Suspans}.docx

./Matthew Scott Hansen:
Matthew Scott Hansen - Ucigasul din umbra 1.0 '{Horror}.docx

./Matt Hilton:
Matt Hilton - Dupa ploaia rosie 1.0 '{SF}.docx

./Mattias Edvardsson:
Mattias Edvardsson - O familie aproape normala 1.0 '{Literatura}.docx

./Maura Petruzzi:
Maura Petruzzi - Primavara in Sardinia 0.99 '{Dragoste}.docx

./Maureen Johnson:
Maureen Johnson - Fata si marea 1.0 '{AventuraTineret}.docx

./Maureen Lindley:
Maureen Lindley - O fata ca tine 0.99 '{Literatura}.docx

./Maurice Baring:
Maurice Baring - Daphne Adeane 1.0 '{Dragoste}.docx

./Maurice Champagne:
Maurice Champagne - Taina farului indoliat 1.0 '{Tineret}.docx

./Maurice Dekobra:
Maurice Dekobra - Gondola cu himere 1.0 '{Literatura}.docx

./Maurice de Moulins:
Maurice de Moulins - Spanzuratul de la Rachul 27 1.0 '{Western}.docx

./Maurice Druon:
Maurice Druon - Regii Blestemati V1 - Regele de fier 1.0 '{ClasicSt}.docx
Maurice Druon - Regii Blestemati V2 - Regina sugrumata 1.0 '{ClasicSt}.docx
Maurice Druon - Regii Blestemati V3 - Otravurile coroanei 1.0 '{ClasicSt}.docx
Maurice Druon - Regii Blestemati V4 - Legea barbatilor 1.0 '{ClasicSt}.docx
Maurice Druon - Regii Blestemati V5 - Lupoaica Frantei 1.0 '{ClasicSt}.docx
Maurice Druon - Regii Blestemati V6 - Crinul si leul 1.0 '{ClasicSt}.docx
Maurice Druon - Regii Blestemati V7 - Cand un rege pierde Franta 1.0 '{ClasicSt}.docx

./Maurice Herzog:
Maurice Herzog - Cucerirea Anapurnei 0.99 '{Calatorii}.docx

./Maurice Leblanc:
Maurice Leblanc - Afacerea Kesselbach 1.0 '{Politista}.docx
Maurice Leblanc - Arsene Lupin contra lui Herlock Sholmes 0.99 '{Politista}.docx
Maurice Leblanc - Arsene Lupin si comoara regilor Frantei 0.99 '{Politista}.docx
Maurice Leblanc - Contesa de Cagliostro 0.9 '{Politista}.docx
Maurice Leblanc - Domnisoara cu ochii verzi 0.9 '{Politista}.docx
Maurice Leblanc - Domnul spargator 0.9 '{Politista}.docx
Maurice Leblanc - Enigma acului scobit 0.9 '{Politista}.docx
Maurice Leblanc - Inelul cu smarald 1.0 '{Politista}.docx
Maurice Leblanc - Misterul de la Barre-Y-Va 1.0 '{Politista}.docx
Maurice Leblanc - Misterul esarfei purpurii 1.0 '{Politista}.docx
Maurice Leblanc - O aventura a lui Arsene Lupin 0.9 '{Politista}.docx
Maurice Leblanc - O crima in pasi de dans 1.0 '{Politista}.docx
Maurice Leblanc - Secretul triunghiului normand 1.0 '{Politista}.docx
Maurice Leblanc - Sfesnicul cu sapte brate 1.0 '{Politista}.docx

./Maurice Renard:
Maurice Renard - Cantareata 0.9 '{SF}.docx
Maurice Renard - Omul cu trup inconsistent 0.99 '{SF}.docx
Maurice Renard - Omul trucat 1.0 '{SF}.docx

./Mauro Raccasi:
Mauro Raccasi - Druidul - V1 Druidul 1.0 '{SF}.docx
Mauro Raccasi - Druidul - V2 Domnia lui Conan 1.0 '{SF}.docx
Mauro Raccasi - Druidul - V3 Luptatorul de la Stonehenge 1.0 '{SF}.docx

./Mavis Heath Miller:
Mavis Heath Miller - Scandal la Crescent 0.99 '{Romance}.docx

./Max Blecher:
Max Blecher - Inimi cicatrizate 0.9 '{Diverse}.docx
Max Blecher - Intamplari in irealitatea imediata 1.0 '{Diverse}.docx
Max Blecher - Vizuina luminata 1.0 '{Diverse}.docx

./Max Brand:
Max Brand - Tigrul 1.0 '{Aventura}.docx

./Max Brooks:
Max Brooks - Razboiul Z 1.0 '{SF}.docx

./Max Frisch:
Max Frisch - Addenda 0.99 '{Literatura}.docx
Max Frisch - Biedermann si incendiatorii 0.9 '{Literatura}.docx
Max Frisch - Don Juan 0.9 '{Literatura}.docx
Max Frisch - Homo Faber 2.0 '{Literatura}.docx
Max Frisch - Nu sunt Stiller 0.6 '{Literatura}.docx

./Max Gallo:
Max Gallo - Crucea occidentului - V1 Intru acest semn vei invinge 1.0 '{AventuraIstorica}.docx
Max Gallo - Crucea occidentului - V2 Parisul in rugaciune 1.0 '{AventuraIstorica}.docx
Max Gallo - Ludovic al XIV-lea - V1 Regele Soare 1.0 '{AventuraIstorica}.docx
Max Gallo - Ludovic al XIV-lea - V2 Iarna marelui rege 1.0 '{AventuraIstorica}.docx
Max Gallo - Romanii - V1 Spartacus - Revolta sclavilor 1.0 '{AventuraIstorica}.docx
Max Gallo - Romanii - V2 Nero - Domnia antihristului 1.0 '{AventuraIstorica}.docx
Max Gallo - Romanii - V3 Titus - Martiriul evreilor 1.0 '{AventuraIstorica}.docx

./Maxime Delamare:
Maxime Delamare - Triplu salt mortal 1.0 '{Politista}.docx

./Maxim Gorki:
Maxim Gorki - Foma Gordeev 2.0 '{ClasicSt}.docx
Maxim Gorki - Mama 2.0 '{ClasicSt}.docx
Maxim Gorki - V1 Copilaria 2.0 '{ClasicSt}.docx
Maxim Gorki - V2 La stapan 2.0 '{ClasicSt}.docx
Maxim Gorki - V3 Universitatile mele 2.0 '{ClasicSt}.docx

./Maxine Barry:
Maxine Barry - Dezbinare 0.99 '{Literatura}.docx

./Max Seydlitz:
Max Seydlitz - Noaptea cea mai lunga 1.0 '{ActiuneRazboi}.docx

./Maya Banks:
Maya Banks - Dulce - V1 Dulce supunere 1.0 '{Erotic}.docx
Maya Banks - Dulce - V2 Dulce abandon 1.0 '{Erotic}.docx

./Maya Niculescu:
Maya Niculescu - Statuia lui Ahile 1.0 '{ClubulTemerarilor}.docx

./Mayne Reid:
Mayne Reid - Calaretul fara cap 2.0 '{Western}.docx

./Maysie Grieg:
Maysie Grieg - Sfidand pericolul 0.99 '{Romance}.docx

./Mazo de la Roche:
Mazo de la Roche - Jalna - V1 1.0 '{AventuraIstorica}.docx
Mazo de la Roche - Jalna - V11 Un nou inceput 2.0 '{AventuraIstorica}.docx
Mazo de la Roche - Jalna - V12 Dimineti la Jalna 2.0 '{AventuraIstorica}.docx
Mazo de la Roche - Jalna - V13 Mary Wakefield 2.0 '{AventuraIstorica}.docx
Mazo de la Roche - Jalna - V14 Tineretea lui Renny 2.0 '{AventuraIstorica}.docx

./Megan Crewe:
Megan Crewe - Mai las-o moale cu fantomele 1.0 '{SF}.docx

./Meg Gardiner:
Meg Gardiner - Lantul de crime 1.0 '{Politista}.docx

./Meg Wisgate:
Meg Wisgate - Liniile vietii 0.9 '{Romance}.docx

./Meg Wolitzer:
Meg Wolitzer - Interesantii 1.0 '{Literatura}.docx
Meg Wolitzer - Puterea femeilor 1.0 '{Literatura}.docx
Meg Wolitzer - Sotia 1.0 '{Literatura}.docx

./Meik Wiking:
Meik Wiking - Mica enciclopedie Lykke - In cautarea celor mai fericiti oameni din lume 1.0 '{DezvoltarePersonala}.docx

./Meister Eckhart:
Meister Eckhart - Desprinde-te 0.6 '{Spiritualitate}.docx
Meister Eckhart - Invataturi spirituale 0.9 '{Spiritualitate}.docx

./Melania Cuc:
Melania Cuc - Dantela de Babilon 0.8 '{Diverse}.docx
Melania Cuc - Miercurea din cenusa 0.9 '{Diverse}.docx

./Melania Mazzucco:
Melania Mazzucco - O zi perfecta 1.0 '{Politista}.docx

./Melanie Hurst:
Melanie Hurst - Cantecul sicomorului 0.99 '{Dragoste}.docx
Melanie Hurst - Misiune indeplinita 0.8 '{Dragoste}.docx
Melanie Hurst - Profesorul de schi 0.99 '{Romance}.docx

./Melfior Ra:
Melfior Ra - Calatorii pe Vamfim 0.8 '{Spiritualitate}.docx

./Melina Marchetta:
Melina Marchetta - Salvand-o pe Francesca 1.0 '{Literatura}.docx

./Melinda Harris:
Melinda Harris - Imbratisarea vantului 0.99 '{Dragoste}.docx

./Melissa de la Cruz:
Melissa de la Cruz - Ashley - V1 O noua senzatie in scoala 1.0 '{AventuraTineret}.docx
Melissa de la Cruz - Ashley - V2 Nu-i asa ca esti geloasa 0.9 '{AventuraTineret}.docx
Melissa de la Cruz - Ashley - V3 Aniversare cu nabadai 1.0 '{AventuraTineret}.docx
Melissa de la Cruz - Ashley - V4 Gloss, Glamour si o jungla de intrigi 1.0 '{AventuraTineret}.docx
Melissa de la Cruz - Sange albastru - V1 Sange albastru 0.6 '{AventuraTineret}.docx
Melissa de la Cruz - Sange albastru - V2 Bal mascat 1.0 '{AventuraTineret}.docx
Melissa de la Cruz - Sange albastru - V3 Revelatii 1.0 '{AventuraTineret}.docx
Melissa de la Cruz - Sange albastru - V4 Mostenirea familiei Van Alen 0.99 '{AventuraTineret}.docx
Melissa de la Cruz - Sange albastru - V5 Bloody Valentine 1.0 '{AventuraTineret}.docx

./Melissa Marr:
Melissa Marr - Fascinatie - V1 Fascinatie 1.0 '{Supranatural}.docx
Melissa Marr - Fascinatie - V2 Obsesie 1.0 '{Supranatural}.docx

./Melodie Adams:
Melodie Adams - Plan de joc 1.0 '{Romance}.docx
Melodie Adams - Stralucirea unei flacari 1.0 '{Romance}.docx
Melodie Adams - Vara indiana 1.0 '{Romance}.docx

./Meredith Roselli:
Meredith Roselli - Limpede precum cristalul 1.0 '{Dragoste}.docx

./Meredith Wild:
Meredith Wild - Hacker - V1 Intalnire periculoasa 0.9 '{Erotic}.docx
Meredith Wild - Hacker - V2 Atractie fatala 0.9 '{Erotic}.docx
Meredith Wild - Hacker - V3 Pasiune ametitoare 0.9 '{Erotic}.docx
Meredith Wild - Hacker - V4 Legatura fragila 0.9 '{Erotic}.docx
Meredith Wild - Hacker - V5 Iubire triumfatoare 1.0 '{Erotic}.docx
Meredith Wild - Misadventures - V4 Aventurile unei fete de la oras 1.0 '{Erotic}.docx
Meredith Wild - Misadventures - V6 Aventurile unei sotii cuminti 1.0 '{Erotic}.docx

./Mesa Selimovic:
Mesa Selimovic - Dervisul si moartea 1.0 '{Literatura}.docx

./Mia Sheridan:
Mia Sheridan - Vocea lui Archer 1.0 '{Romance}.docx

./Michael A. Stackpole:
Michael A. Stackpole - Epoca Descoperirilor - V1 Atlasul secret 1.0 '{SF}.docx
Michael A. Stackpole - Epoca Descoperirilor - V2 Hartile prevestitoare 1.0 '{SF}.docx
Michael A. Stackpole - Epoca Descoperirilor - V3 Lumea noua 1.0 '{SF}.docx
Michael A. Stackpole - Talion Revenantul 1.0 '{SF}.docx

./Michael Baden & Linda Kenney:
Michael Baden & Linda Kenney - Tacerea 1.0 '{Thriller}.docx

./Michael Baigent:
Michael Baigent - Dosarele Isus 1.0 '{MistersiStiinta}.docx
Michael Baigent - Sangele sfant si sfantul Graal 0.8 '{MistersiStiinta}.docx

./Michael Blake:
Michael Blake - Danseaza cu lupii 2.1 '{Western}.docx

./Michael Byrnes:
Michael Byrnes - Secretul lui Iosif 0.9 '{AventuraIstorica}.docx

./Michael Connelly:
Michael Connelly - Ancheta 1.0 '{Politista}.docx
Michael Connelly - Avocatul din limuzina 1.0 '{Politista}.docx
Michael Connelly - Bufnita de plastic 1.0 '{Politista}.docx
Michael Connelly - Darling Lilly 1.0 '{Politista}.docx
Michael Connelly - Harry Bosch - V2 Chipul dur al despartirii 1.0 '{Politista}.docx
Michael Connelly - Harry Bosch - V12 Echo Park 0.6 '{Politista}.docx
Michael Connelly - Jocul de Domino 1.0 '{Politista}.docx
Michael Connelly - Jocuri de culise 1.0 '{Politista}.docx
Michael Connelly - Pana la capat 1.0 '{Politista}.docx
Michael Connelly - Piste false 1,0 '{Politista}.docx
Michael Connelly - Ultimul autograf 1.0 '{Politista}.docx
Michael Connelly - Verdictul plumbului 0.8 '{Politista}.docx

./Michael Cordy:
Michael Cordy - Codul lui Isus 1.0 '{Thriller}.docx
Michael Cordy - Lucifer 1.0 '{Thriller}.docx

./Michael Cox:
Michael Cox - Sensul noptii 0.99 '{Literatura}.docx

./Michael Crichton:
Michael Crichton - Calomnia 1.0 '{Horror}.docx
Michael Crichton - Congo 1.1 '{Horror}.docx
Michael Crichton - Frica 1.0 '{Horror}.docx
Michael Crichton - Germenul Andromeda 1.0 '{Horror}.docx
Michael Crichton - Jurasic Parc 0.99 '{Horror}.docx
Michael Crichton - Lumea pierduta 1.1 '{Horror}.docx
Michael Crichton - Next 1.1 '{Horror}.docx
Michael Crichton - Noaptea samurailor 2.1 '{Horror}.docx
Michael Crichton - Prada 1.1 '{Horror}.docx
Michael Crichton - Sfera 2.1 '{Horror}.docx
Michael Crichton - Spitalul de urgenta 1.0 '{Horror}.docx
Michael Crichton - Terminalul uman 1.1 '{Horror}.docx

./Michael Cunningham:
Michael Cunningham - Orele 1.0 '{Literatura}.docx

./Michael Curtis Ford:
Michael Curtis Ford - Caderea Romei 1.0 '{AventuraIstorica}.docx
Michael Curtis Ford - Sabia lui Attila 1.0 '{AventuraIstorica}.docx

./Michael Dobbs:
Michael Dobbs - House of Cards - V1 Culisele puterii 1.0 '{Politista}.docx

./Michael E. Gerber:
Michael E. Gerber - Mitul intreprinzatorului 1.0 '{DezvoltarePersonala}.docx

./Michael Ende:
Michael Ende - Momo 0.99 '{SF}.docx
Michael Ende - Povestea fara sfarsit 0.6 '{SF}.docx

./Michael F. Flynn:
Michael F. Flynn - Cand Dumnezeu a batut din palme 0.2 '{SF}.docx
Michael F. Flynn - Casa viselor 0.9 '{SF}.docx

./Michael Grant:
Michael Grant - Disparuti - V1 Disparuti 1.0 '{SF}.docx
Michael Grant - Disparuti - V2 Foamea 1.0 '{SF}.docx
Michael Grant - Disparuti - V3 Minciunile 1.0 '{SF}.docx

./Michael H. Payne:
Michael H. Payne - Punga 0.99 '{SF}.docx

./Michael Hartland:
Michael Hartland - Anul scorpionului 1.0 '{Thriller}.docx
Michael Hartland - A treia tradare 1.0 '{Thriller}.docx

./Michael Hjorth:
Michael Hjorth - Sebastian Bergman - V1 Barbatul care nu era ucigas 1.0 '{Thriller}.docx

./Michael Innes:
Michael Innes - Misterul manuscrisului 1.0 '{Politista}.docx

./Michael Koryta:
Michael Koryta - Lincoln Perry - V1 Descoperiri uimitoare 1.0 '{Literatura}.docx

./Michael Laitman:
Michael Laitman - Atingerea lumilor spirituale 0.8 '{Spiritualitate}.docx

./Michael Lawson:
Michael Lawson - Cercul interior 1.0 '{Thriller}.docx

./Michael Lukas:
Michael Lukas - Oracolul din Stambul 1.0 '{Aventura}.docx

./Michael Marshall:
Michael Marshall - Oamenii de paie 1.0 '{Thriller}.docx

./Michael Moorcock:
Michael Moorcock - Elric la sfarsitul timpului 1.0 '{CalatorieinTimp}.docx
Michael Moorcock - Schoonerul ghetii 0.9 '{SF}.docx

./Michael Morpurgo:
Michael Morpurgo - Calul de razboi 1.0 '{Razboi}.docx
Michael Morpurgo - Un vultur in zapada 1.0 '{Razboi}.docx

./Michael Newton:
Michael Newton - V1 Calatoria sufletelor 1.0 '{Spiritualitate}.docx
Michael Newton - V2 Destinul sufletelor 1.0 '{Spiritualitate}.docx
Michael Newton - V3 Viata dintre vieti 1.0 '{Spiritualitate}.docx

./Michael Ondaatje:
Michael Ondaatje - Pacientul englez 0.99 '{Literatura}.docx

./Michael P. Kube Mcdowell:
Michael P. Kube Mcdowell - Slac 0.99 '{SF}.docx

./Michael Palmer:
Michael Palmer - A cincea fiola 0.9 '{Suspans}.docx
Michael Palmer - Freud si Jung despre religie 0.9 '{Filozofie}.docx
Michael Palmer - Leacul miraculos 1.0 '{Suspans}.docx
Michael Palmer - Primul pacient 1.0 '{Suspans}.docx

./Michael Peinkofer:
Michael Peinkofer - Fratia runelor 1.0 '{AventuraIstorica}.docx
Michael Peinkofer - In umbra lui Thot 1.0 '{AventuraIstorica}.docx

./Michael Pitre:
Michael Pitre - 5-uri si 25-uri 1.0 '{Suspans}.docx

./Michael Punke:
Michael Punke - Razbunatorul. Legenda lui Hugh Glass 1.0 '{Aventura}.docx

./Michael Robotham:
Michael Robotham - Suspectul 1.0 '{Thriller}.docx

./Michael Scott:
Michael Scott - Secretul Nemuritorului Nicholas Flamel - V1 Alchimistul 1.0 '{Aventura}.docx
Michael Scott - Secretul Nemuritorului Nicholas Flamel - V2 Magicianul 1.0 '{Aventura}.docx

./Michael Shermer:
Michael Shermer - De ce cred oamenii in bazaconii 1.0 '{Psihologie}.docx

./Michael Sinclair:
Michael Sinclair - O lunga hibernare 1.0 '{Politista}.docx
Michael Sinclair - Pactul dolarului 1.0 '{Politista}.docx

./Michael Swanwick:
Michael Swanwick - Ginungagap 0.99 '{SF}.docx
Michael Swanwick - Mlastina de cositor 2.0 '{SF}.docx
Michael Swanwick - Transmigratia lui Philip K. 0.99 '{SF}.docx

./Michael Swanwick & William Gibson:
Michael Swanwick & William Gibson - Lupta aeriana 1.0 '{SF}.docx

./Michael Teitelbaum:
Michael Teitelbaum - Barbatii in negru 2 1.0 '{SF}.docx

./Michael Tellinger:
Michael Tellinger - Sclavii Zeilor V1 0.9 '{Spiritualitate}.docx
Michael Tellinger - Sclavii Zeilor V2 0.9 '{Spiritualitate}.docx

./Michael Wallner:
Michael Wallner - Aprilie in Paris 1.0 '{Dragoste}.docx

./Michael White:
Michael White - Arta crimei 1.0 '{AventuraIstorica}.docx
Michael White - Echinox 0.9 '{AventuraIstorica}.docx
Michael White - Inelul familiei Borgia 1.0 '{AventuraIstorica}.docx
Michael White - Secretul familiei Medici 1.0 '{AventuraIstorica}.docx

./Michael Winn:
Michael Winn - Calea surasului interior 0.7 '{Spiritualitate}.docx

./Michal Viewegh:
Michal Viewegh - Cum se educa fetele in Boemia 1.0 '{Literatura}.docx

./Michela Murgia:
Michela Murgia - Accabadora 1.0 '{Literatura}.docx

./Michel Balard:
Michel Balard - Cruciadele 1.0 '{Religie}.docx

./Michel Benoit:
Michel Benoit - Dansul raului 1.0 '{Literatura}.docx

./Michel Brice:
Michel Brice - Brigada Mondena - Armasarul din Saumur 1.0 '{ActiuneComando}.docx
Michel Brice - Brigada Mondena - Aventuri in Bulgaria 1.0 '{ActiuneComando}.docx
Michel Brice - Brigada Mondena - Criminala schioapa din Saint-Tropez 1.0 '{ActiuneComando}.docx
Michel Brice - Brigada Mondena - Croaziera fatala 1.0 '{ActiuneComando}.docx
Michel Brice - Brigada Mondena - Domnisoara... de moravuri usoare 1.0 '{ActiuneComando}.docx
Michel Brice - Brigada Mondena - Fiicele monseniorului 1.0 '{ActiuneComando}.docx
Michel Brice - Brigada Mondena - Mica regina a turului Frantei 1.0 '{ActiuneComando}.docx
Michel Brice - Brigada Mondena - Pantera hotelurilor de lux 1.0 '{ActiuneComando}.docx
Michel Brice - Brigada Mondena - Tragatoarea de elita 1.0 '{ActiuneComando}.docx
Michel Brice - Brigada Mondena - Zana cersetorilor 1.0 '{ActiuneComando}.docx
Michel Brice - Brigada mondena 0.6 '{ActiuneComando}.docx

./Michel Bussi:
Michel Bussi - Fetita cu ochi albastri 1.0 '{Aventura}.docx
Michel Bussi - Nuferi negri 1.0 '{Aventura}.docx

./Michel Deon:
Michel Deon - Un taxi mov 0.6 '{Diverse}.docx

./Michel de Saint Pierre:
Michel de Saint Pierre - Acuzata 0.7 '{Thriller}.docx

./Michele Tournier:
Michele Tournier - Amantii taciturni 0.7 '{Literatura}.docx

./Michel Folco:
Michel Folco - Doar Dumnezeu si noi 3.0 '{AventuraIstorica}.docx

./Michel Foucault:
Michel Foucault - Theatrum philosoficum. Eseuri V1 0.9 '{Filozofie}.docx

./Michel Houellebecq:
Michel Houellebecq - Platforma 0.6 '{Romance}.docx

./Michel Lebrun:
Michel Lebrun - Castelul cu monstri 2.0 '{Horror}.docx

./Michelle Cunnah:
Michelle Cunnah - Confesiunile unei iubarete 0.9 '{Tineret}.docx

./Michelle Gable:
Michelle Gable - Un apartament la Paris 0.9 '{Literatura}.docx

./Michelle Harrison:
Michelle Harrison - Thirteen Treasures - V1 Treisprezece comori 1.0 '{Diverse}.docx
Michelle Harrison - Thirteen Treasures - V2 Treisprezece blesteme 1.0 '{Diverse}.docx

./Michelle Hodkin:
Michelle Hodkin - Mara Dyer - V1 Inceputul 1.0 '{SF}.docx
Michelle Hodkin - Mara Dyer - V2 Transformarea 1.0 '{SF}.docx

./Michelle Obama:
Michelle Obama - Povestea mea 1.0 '{Biografie}.docx

./Michelle Paver:
Michelle Paver - Cronici din Tinuturi Intunecate - V1 Fratia lupilor 1.0 '{Supranatural}.docx
Michelle Paver - Cronici din Tinuturi Intunecate - V2 Spiritul calator 1.0 '{Supranatural}.docx
Michelle Paver - Cronici din Tinuturi Intunecate - V3 Devoratorul de suflete 1.0 '{Supranatural}.docx
Michelle Paver - Cronici din Tinuturi Intunecate - V4 Proscrisul 1.0 '{Supranatural}.docx

./Michelle Reid:
Michelle Reid - Cu cartile pe fata 1.0 '{Romance}.docx

./Michelle Richmond:
Michelle Richmond - Pactul conjugal 1.0 '{Diverse}.docx

./Michel Montignac:
Michel Montignac - Ma hranesc, deci slabesc 0.9 '{Sanatate}.docx

./Michel Tournier:
Michel Tournier - Fata si moartea 0.99 '{Literatura}.docx
Michel Tournier - Fecioara si capcaunul 0.99 '{Literatura}.docx
Michel Tournier - Picatura de aur 0.99 '{Literatura}.docx
Michel Tournier - Piticul rosu 0.8 '{Literatura}.docx
Michel Tournier - Regele arinilor 0.9 '{Literatura}.docx
Michel Tournier - Vineri sau viata salbatica 1.0 '{Aventura}.docx

./Michel Zevaco:
Michel Zevaco - Borgia - V1 Crimele familiei Borgia 1.0 '{CapasiSpada}.docx
Michel Zevaco - Borgia - V2 Regele cersetorilor 1.0 '{CapasiSpada}.docx
Michel Zevaco - Borgia - V4 Printesa Rayon D'or 1.0 '{CapasiSpada}.docx
Michel Zevaco - Borgia - V5 Doamna in alb, doamna in negru 1.0 '{CapasiSpada}.docx
Michel Zevaco - Buridan. Regina blestemata 2.0 '{CapasiSpada}.docx
Michel Zevaco - Cavalerul Hardy de Passavant - V1 Iesirea din moarte 1.0 '{CapasiSpada}.docx
Michel Zevaco - Cavalerul Hardy de Passavant - V3 Regina Isabeau 1.0 '{CapasiSpada}.docx
Michel Zevaco - Cavalerul Hardy de Passavant - V4 Cavalerul Passavant 1.0 '{CapasiSpada}.docx
Michel Zevaco - Cavalerul regelui 1.0 '{CapasiSpada}.docx
Michel Zevaco - Curtea miracolelor 1.0 '{CapasiSpada}.docx
Michel Zevaco - Don Juan - V1 Don Juan 1.0 '{CapasiSpada}.docx
Michel Zevaco - Don Juan - V2 Don Juan si comandorul 1.0 '{CapasiSpada}.docx
Michel Zevaco - Don Juan - V3 Regina Argotului V1 2.0 '{CapasiSpada}.docx
Michel Zevaco - Don Juan - V3 Regina Argotului V2 1.0 '{CapasiSpada}.docx
Michel Zevaco - Don Juan - V4 Contele de Montauban 2.0 '{CapasiSpada}.docx
Michel Zevaco - Dragoste si ura 1.0 '{CapasiSpada}.docx
Michel Zevaco - Eroina 1.0 '{CapasiSpada}.docx
Michel Zevaco - Marie Rose - V1 Micuta din nord 3.0 '{CapasiSpada}.docx
Michel Zevaco - Marie Rose - V2 Marie Rose razbunata 1.0 '{CapasiSpada}.docx
Michel Zevaco - Nostradamus - V1 Nostradamus, regele intunericului 1.0 '{CapasiSpada}.docx
Michel Zevaco - Nostradamus - V2 Fiul lui Nostradamus 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pampadour - V1 Marchiza de Pampadour 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pampadour - V2 Rivalul regelui 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pardaillan - V1 Cavalerii Pardaillan V1 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pardaillan - V1 Cavalerii Pardaillan V2 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pardaillan - V2 Epopeea dragostei 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pardaillan - V3 Fausta 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pardaillan - V4 Fausta invinsa 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pardaillan - V5 Pardaillan si Fausta 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pardaillan - V6 Iubirile lui Chico 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pardaillan - V7 Fiul lui Pardaillan 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pardaillan - V8 Comoara Faustei 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pardaillan - V9 Sfarsitul lui Pardaillan 1.0 '{CapasiSpada}.docx
Michel Zevaco - Pardaillan - V10 Sfarsitul Faustei 1.0 '{CapasiSpada}.docx
Michel Zevaco - Poveste de dragoste 1.0 '{CapasiSpada}.docx
Michel Zevaco - Puntea suspinelor V1 4.0 '{CapasiSpada}.docx
Michel Zevaco - Puntea suspinelor V2 4.0 '{CapasiSpada}.docx
Michel Zevaco - Puntea suspinelor V3 4.0 '{CapasiSpada}.docx
Michel Zevaco - Puntea suspinelor V4 4.0 '{CapasiSpada}.docx

./Mickey Ferron:
Mickey Ferron - Legea junglei 1.0 '{SF}.docx

./Miguel Angel Asturias:
Miguel Angel Asturias - Domnul presedinte 2.0 '{Thriller}.docx
Miguel Angel Asturias - Oameni de porumb 0.9 '{Literatura}.docx

./Miguel Collazo:
Miguel Collazo - Cartea fantastica a lui Oaj 1.0 '{SF}.docx

./Miguel de Cervantes:
Miguel de Cervantes - Don Quijote de la Mancha V1 1.0 '{ClasicSt}.docx
Miguel de Cervantes - Don Quijote de la Mancha V2 1.0 '{ClasicSt}.docx
Miguel de Cervantes - Galateea 0.99 '{ClasicSt}.docx
Miguel de Cervantes - Muncile lui Persiles si ale Sigismundei 1.0 '{ClasicSt}.docx

./Miguel Delibes:
Miguel Delibes - Ereticul 0.99 '{Literatura}.docx

./Miguel de Unamuno:
Miguel de Unamuno - Trei nuvele exemplare si un prolog 0.99 '{Literatura}.docx

./Mihaela Gaisteanu:
Mihaela Gaisteanu - Psihologia copilului 0.7 '{Copii}.docx

./Mihaela Radulescu:
Mihaela Radulescu - Despre lucrurile simple 0.9 '{Diverse}.docx

./Mihai Alexandru:
Mihai Alexandru - In martie 0.9 '{ProzaScurta}.docx

./Mihai Baiu:
Mihai Baiu - Neclintitii 1.0 '{IstoricaRo}.docx

./Mihai Dragolea:
Mihai Dragolea - Calatorii spre muche de cutit 0.99 '{ProzaScurta}.docx

./Mihai Eminescu:
Mihai Eminescu - Articole politice 1.0 '{ClasicRo}.docx
Mihai Eminescu - Avatarii faraonului Tla 0.9 '{ClasicRo}.docx
Mihai Eminescu - Ce te legeni 0.99 '{ClasicRo}.docx
Mihai Eminescu - Cezara si alte nuvele 1.0 '{ClasicRo}.docx
Mihai Eminescu - Fat-Frumos din lacrima 1.0 '{ClasicRo}.docx
Mihai Eminescu - Fat-Frumos din lacrima si alte basme 1.0 '{ClasicRo}.docx
Mihai Eminescu - Frumoasa lumii 1.0 '{ClasicRo}.docx
Mihai Eminescu - Geniu pustiu 1.0 '{ClasicRo}.docx
Mihai Eminescu - La aniversara 1.0 '{ClasicRo}.docx
Mihai Eminescu - Luceafarul 1.0 '{ClasicRo}.docx
Mihai Eminescu - Poezii 0.9 '{ClasicRo}.docx
Mihai Eminescu - Poezii 1866-1874 0.99 '{ClasicRo}.docx
Mihai Eminescu - Poeziile editiei princeps 0.99 '{ClasicRo}.docx
Mihai Eminescu - Povestea magului calator in stele 1.0 '{ClasicRo}.docx
Mihai Eminescu - Povesti 1.0 '{ClasicRo}.docx
Mihai Eminescu - Proza 0.9 '{ClasicRo}.docx
Mihai Eminescu - Sarmanul Dionis 1.0 '{ClasicRo}.docx
Mihai Eminescu - Scrisori 1.0 '{ClasicRo}.docx
Mihai Eminescu - Scrisori alese din corespondenta lui Eminescu 1.0 '{ClasicRo}.docx
Mihai Eminescu - Spovedanie 0.99 '{Necenzurata}.docx

./Mihai Gainusa:
Mihai Gainusa - Fara cap si fara coada 1.0 '{Umor}.docx

./Mihai Giugariu:
Mihai Giugariu - Magdalena 1.0 '{Dragoste}.docx

./Mihai Gramescu:
Mihai Gramescu - Molia 5.0 '{SF}.docx

./Mihail Bulgakov:
Mihail Bulgakov - Cate enciclopedii Blochaus 0.99 '{Literatura}.docx
Mihail Bulgakov - Craii 0.99 '{Literatura}.docx
Mihail Bulgakov - Garson, inc-o enciclopedie 0.99 '{Literatura}.docx
Mihail Bulgakov - Maestrul si Margareta 1.0 '{Literatura}.docx
Mihail Bulgakov - O noua modalitate de raspandire a cartii 0.9 '{Literatura}.docx
Mihail Bulgakov - Roman teatral 0.99 '{Literatura}.docx
Mihail Bulgakov - Zalogul de dragoste 0.99 '{Literatura}.docx

./Mihail Calmacu:
Mihail Calmacu - Sub semnul hangerului V1 1.0 '{ClubulTemerarilor}.docx
Mihail Calmacu - Sub semnul hangerului V2 1.0 '{ClubulTemerarilor}.docx

./Mihail Celarianu:
Mihail Celarianu - Diamant verde 0.8 '{Literatura}.docx

./Mihail Crama:
Mihail Crama - Calator spre portile asfintitului 0.9 '{Diverse}.docx

./Mihail Drumes:
Mihail Drumes - Cazul Magheru 1.0 '{AventuraTineret}.docx
Mihail Drumes - Elevul Dima dintr-a saptea 1.0 '{AventuraTineret}.docx
Mihail Drumes - Invitatie la vals 2.0 '{AventuraTineret}.docx
Mihail Drumes - Povestea neamului romanesc 0.9 '{IstoricaRo}.docx
Mihail Drumes - Scrisoare de dragoste 2.0 '{AventuraTineret}.docx

./Mihail Gramescu:
Mihail Gramescu - Aporisticon 0.5 '{SF}.docx
Mihail Gramescu - Comunicare prin juxtapunere 1.0 '{SF}.docx
Mihail Gramescu - Fictiuni 1.0 '{SF}.docx
Mihail Gramescu - Ipoteza neortodoxa 0.99 '{SF}.docx
Mihail Gramescu - Moara de apa 1.0 '{SF}.docx
Mihail Gramescu - Molia 5.0 '{SF}.docx
Mihail Gramescu - Prietenul 1.0 '{SF}.docx
Mihail Gramescu - Saritorii in gol 1.0 '{SF}.docx
Mihail Gramescu - Scena cu salonul 0.7 '{SF}.docx
Mihail Gramescu - Sfidarea 2.0 '{SF}.docx

./Mihail Heyfetz:
Mihail Heyfetz - Secretarul politiei secrete1.0 '{Politista}.docx

./Mihail Joldea:
Mihail Joldea - Calea robilor, cumpana vulturilor 5.0 '{IstoricaRo}.docx
Mihail Joldea - Cei sase de la Plevna 5.0 '{IstoricaRo}.docx
Mihail Joldea - Tradatorul Mariei sale 5.0 '{IstoricaRo}.docx

./Mihail Kogalniceanu:
Mihail Kogalniceanu - Adunari dantuitoare 1.0 '{ClasicRo}.docx
Mihail Kogalniceanu - Iluzii pierdute 1.0 '{ClasicRo}.docx
Mihail Kogalniceanu - Tainele inimii 0.9 '{ClasicRo}.docx

./Mihail Puhov:
Mihail Puhov - Cu racheta de ocazie 1.0 '{SF}.docx
Mihail Puhov - Salvarea vietii 0.99 '{SF}.docx

./Mihail Sadoveanu:
Mihail Sadoveanu - Amintirile caprarului Gheorghita 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Baltagul 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Cazul Eugenitei Costea 2.0 '{ClasicRo}.docx
Mihail Sadoveanu - Cozma Racoare 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Creanga de aur 2.0 '{ClasicRo}.docx
Mihail Sadoveanu - Cuibul invaziilor V1 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Cuibul invaziilor V2 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Demonul tineretii 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Departari 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Domnu Trandafir 0.9 '{ClasicRo}.docx
Mihail Sadoveanu - Dumbrava minunata 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Fratii Jderi - V1 Ucenicia lui Ionut 2.0 '{ClasicRo}.docx
Mihail Sadoveanu - Fratii Jderi - V2 Izvorul alb 2.0 '{ClasicRo}.docx
Mihail Sadoveanu - Fratii Jderi - V3 Oamenii Mariei sale 2.0 '{ClasicRo}.docx
Mihail Sadoveanu - Hanu Ancutei 1.9 '{ClasicRo}.docx
Mihail Sadoveanu - Istorisiri vechi si noua 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Judet al sarmanilor 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Locul unde nu s-a intamplat nimic 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Morminte, lacrimile ieromonahului Veniamin 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Nada florilor 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Neamul Soimarestilor 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Nicoara Potcoava 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Noptile de sanziene 1.5 '{ClasicRo}.docx
Mihail Sadoveanu - Nunta domnitei Ruxanda 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Povestiri 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Povestiri din razboi 2.2 '{ClasicRo}.docx
Mihail Sadoveanu - Printre gene 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Soarele in balta sau aventurile sahului 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Soimii 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Strada Lapusneanu. Oameni din luna. Morminte 0.9 '{ClasicRo}.docx
Mihail Sadoveanu - Tara de dincolo de negura 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Uvar 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Venea o moara pe Siret 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Viata lui Stefan cel Mare 1.0 '{ClasicRo}.docx
Mihail Sadoveanu - Zodia cancerului sau vremea Ducai Voda 1.0 '{ClasicRo}.docx

./Mihail Sebastian:
Mihail Sebastian - De doua mii de ani. Cum am devenit huligan 1.0 '{Literatura}.docx
Mihail Sebastian - Femei 1.0 '{ClasicRo}.docx
Mihail Sebastian - Jocul de-a vacanta 2.0 '{Teatru}.docx
Mihail Sebastian - Jurnal (1935-1944) 1.0 '{ClasicRo}.docx
Mihail Sebastian - Orasul cu salcami. Accidentul 1.1 '{ClasicRo}.docx
Mihail Sebastian - Steaua fara nume 0.8 '{Teatru}.docx
Mihail Sebastian - Ultima ora 1.0 '{Teatru}.docx

./Mihail Siskin:
Mihail Siskin - Parul Venerei 1.0 '{Literatura}.docx

./Mihail Solohov:
Mihail Solohov - Donul linistit V1,4 2.0 '{ClasicSt}.docx
Mihail Solohov - Pamant destelenit V1 0.9 '{ClasicSt}.docx
Mihail Solohov - Pamant destelenit V2 0.9 '{ClasicSt}.docx
Mihail Solohov - Povestiri de pe Don. Soarta unui om 1.0 '{ClasicSt}.docx

./Mihail Tanase:
Mihail Tanase - In ajun de sarbatori 0.7 '{Diverse}.docx

./Mihail Zoscenko:
Mihail Zoscenko - Galosul 0.99 '{Diverse}.docx

./Mihai Mateiu:
Mihai Mateiu - Oameni 0.9 '{ProzaScurta}.docx

./Mihai Moldovan:
Mihai Moldovan - Aruna si cartea obsesie 0.99 '{Diverse}.docx

./Mihai Opris & Vasile Chirita:
Mihai Opris & Vasile Chirita - Auzit-ati d-un Jian 1.0 '{IstoricaRo}.docx

./Mihai Radulescu:
Mihai Radulescu - Decameronul din Nowhershire 0.9 '{Diverse}.docx
Mihai Radulescu - Impuscarea calaretului 0.9 '{Diverse}.docx
Mihai Radulescu - Istoria literaturii de detentie la romani 0.8 '{Inchisoare}.docx
Mihai Radulescu - Tristeti si gratii 0.9 '{Comunism}.docx

./Mihai Rauta:
Mihai Rauta - Calul si noi 5.0 '{SF}.docx

./Mihaita Mihai Loviste:
Mihaita Mihai Loviste - Teodora 0.99 '{Diverse}.docx

./Mihai Tican Rumano:
Mihai Tican Rumano - Lacul cu elefanti 0.8 '{Diverse}.docx

./Mihai Urzica:
Mihai Urzica - Biserica si viermii cei neadormiti 0.9 '{Religie}.docx

./Mihai Victor Stoica:
Mihai Victor Stoica - El si jungla 5.0 '{SF}.docx

./Mihnea Gheorghiu:
Mihnea Gheorghiu - Muschetarul lui Cantemir 1.0 '{IstoricaRo}.docx

./Mihnea Moisescu:
Mihnea Moisescu - Comoara astronautilor 1.0 '{SF}.docx
Mihnea Moisescu - Cosmonautul de piatra 1.0 '{SF}.docx
Mihnea Moisescu - Glasul din pulberea aurie 1.0 '{SF}.docx
Mihnea Moisescu - Intoarcere pe tarmul disparut 1.2 '{SF}.docx

./Mihu Dragomir:
Mihu Dragomir - Povestiri deocamdata fantastice 1.0 '{SF}.docx

./Mika Waltari:
Mika Waltari - Amantii din Bizant 1.0 '{AventuraIstorica}.docx
Mika Waltari - Egipteanul 2.1 '{AventuraIstorica}.docx
Mika Waltari - Etruscul 1.0 '{AventuraIstorica}.docx
Mika Waltari - Femeia si strainul 1.0 '{AventuraIstorica}.docx
Mika Waltari - Mikael Karvajalka 1.0 '{AventuraIstorica}.docx
Mika Waltari - Papillon 0.99 '{AventuraIstorica}.docx
Mika Waltari - Romanul 1.0 '{AventuraIstorica}.docx
Mika Waltari - Secretul imparatiei 1.0 '{AventuraIstorica}.docx

./Mike Conner:
Mike Conner - Cainele calauza 1.0 '{SF}.docx

./Mike Hassel:
Mike Hassel - Cecenia, chip inlacrimat 1.0 '{ActiuneComando}.docx
Mike Hassel - Conspiratia sfarsitului lumii 1.0 '{ActiuneComando}.docx
Mike Hassel - Jurnalul unui mercenar 1.0 '{ActiuneComando}.docx
Mike Hassel - Kosovo. Drumul Golgotei 1.0 '{ActiuneComando}.docx
Mike Hassel - Moartea s-a nascut la Sarajevo 1.0 '{ActiuneComando}.docx
Mike Hassel - Onor la general 1.0 '{ActiuneComando}.docx
Mike Hassel - Ordon, ucide-ti aproapele! 1.0 '{ActiuneComando}.docx
Mike Hassel - Pentru bani, inainte! 1.0 '{ActiuneComando}.docx
Mike Hassel - Razboi fara arme 1.0 '{ActiuneComando}.docx
Mike Hassel - Spaima si moarte in Transnistria 1.0 '{ActiuneComando}.docx

./Mike Resnick:
Mike Resnick - Barnaby in exil 0.99 '{SF}.docx
Mike Resnick - Cele 45 de dinastii antariene 0.99 '{SF}.docx

./Mikkel Birkegaard:
Mikkel Birkegaard - Biblioteca umbrelor 1.0 '{Politista}.docx
Mikkel Birkegaard - Condamnat la moarte 1.0 '{Politista}.docx

./Milan Kundera:
Milan Kundera - Cartea rasului si a uitarii 0.99 '{Literatura}.docx
Milan Kundera - Cortina 0.99 '{Literatura}.docx
Milan Kundera - Gluma 0.99 '{Literatura}.docx
Milan Kundera - Identitatea 0.99 '{Literatura}.docx
Milan Kundera - Ignoranta 0.99 '{Literatura}.docx
Milan Kundera - Insuportabila usuratate a fiintei 0.99 '{Literatura}.docx
Milan Kundera - Iubiri caraghioase 0.99 '{Literatura}.docx
Milan Kundera - Lentoarea 0.99 '{Literatura}.docx
Milan Kundera - Nemurirea 0.99 '{Literatura}.docx
Milan Kundera - O intalnire 0.99 '{Literatura}.docx
Milan Kundera - Sarbatoarea neinsemnatatii 0.99 '{Literatura}.docx
Milan Kundera - Testamente tradate 0.99 '{Literatura}.docx
Milan Kundera - Valsul de adio 1.0 '{Literatura}.docx
Milan Kundera - Viata e in alta parte 1.0 '{Literatura}.docx

./Milan Varos:
Milan Varos - Soarta comorilor de aur 1.0 '{Istorie}.docx

./Mildred Davis:
Mildred Davis - Dragoste fara limite 0.99 '{Dragoste}.docx
Mildred Davis - Pana la delir 0.9 '{Dragoste}.docx
Mildred Davis - Umbra dragostei 0.99 '{Dragoste}.docx
Mildred Davis - Un Craciun neobisnuit 0.99 '{Romance}.docx
Mildred Davis - Un cuplu de pomina 0.99 '{Dragoste}.docx

./Milivoj Andjelkovic:
Milivoj Andjelkovic - Intoarcerea de pe Planeta Ei Bi 0.99 '{SF}.docx

./Milorad Pavic:
Milorad Pavic - Mantia de stele - Ghid astrologic de ghicit 1.0 '{Literatura}.docx

./Miloslav Stingl:
Miloslav Stingl - Aventura marilor capetenii indiene 1.0 '{Civilizatii}.docx

./Milovan Djilas:
Milovan Djilas - Intalniri cu Stalin 0.7 '{Comunism}.docx

./Milton G. Lehrer:
Milton G. Lehrer - Ardealul pamant romanesc 2.0 '{Istorie}.docx

./Mimi Cornel Branescu:
Mimi Cornel Branescu - Bigudiuri 0.9 '{Teatru}.docx
Mimi Cornel Branescu - Daca ala e cu aia 0.9 '{Teatru}.docx
Mimi Cornel Branescu - Dumnezeul de a doua zi 0.7 '{Teatru}.docx
Mimi Cornel Branescu - Gunoierul 0.99 '{Teatru}.docx
Mimi Cornel Branescu - Ieri am trait o zi neagra 0.99 '{Teatru}.docx
Mimi Cornel Branescu - Zmeii 0.99 '{Teatru}.docx

./Mingmei Yip:
Mingmei Yip - Pavilionul Placerilor 0.8 '{Literatura}.docx

./Min Jin Lee:
Min Jin Lee - Pachinko 1.0 '{Literatura}.docx

./Mioara Cremene:
Mioara Cremene - Marirea si decaderea planetei Globus 2.0 '{SF}.docx

./Mioara Musteata:
Mioara Musteata - Kadaon 0.99 '{SF}.docx

./Miranda Lee:
Miranda Lee - Dragoste de vanzare 0.99 '{Dragoste}.docx

./Mircea Berinde:
Mircea Berinde - Paravanul venetian 0.6 '{ClasicRo}.docx

./Mircea Brates:
Mircea Brates - Proxima Centauri 0.9 '{SF}.docx

./Mircea Carbunaru:
Mircea Carbunaru - Colectionarul 0.99 '{SF}.docx

./Mircea Cartarescu:
Mircea Cartarescu - Camifs 0.9 '{Literatura}.docx
Mircea Cartarescu - Cateva poezii de dragoste 0.8 '{Versuri}.docx
Mircea Cartarescu - De ce iubim femeile 0.8 '{Literatura}.docx
Mircea Cartarescu - Nostalgia 0.99 '{Literatura}.docx
Mircea Cartarescu - Orbitor 0.9 '{Literatura}.docx
Mircea Cartarescu - Ruletistul 0.99 '{Literatura}.docx
Mircea Cartarescu - Solenoid 1.0 '{Literatura}.docx

./Mircea Coman:
Mircea Coman - Gradina Yasminei 0.99 '{Diverse}.docx

./Mircea Daneliuc:
Mircea Daneliuc - Apa din cizme 0.8 '{Necenzurat}.docx
Mircea Daneliuc - Cele ce plutesc 0.9 '{Literatura}.docx
Mircea Daneliuc - Strigoi fara tara 0.8 '{Necenzurat}.docx

./Mircea Danieliuc:
Mircea Danieliuc - Ora lanti 0.7 '{Literatura}.docx

./Mircea Deac:
Mircea Deac - Pendula nu merge inapoi 1.0 '{Dragoste}.docx

./Mircea Diaconu:
Mircea Diaconu - La noi cand vine iarna 0.9 '{Literatura}.docx

./Mircea Dinescu:
Mircea Dinescu - La dispozitia dumneavoastra 1.0 '{Literatura}.docx

./Mircea Dogaru:
Mircea Dogaru - Nato si criminalii Grofi Wass de Taga 0.99 '{Istorie}.docx

./Mircea Eliade:
Mircea Eliade - 19 trandafiri 1.0 '{ClasicRo}.docx
Mircea Eliade - Coloana nesfarsita 0.99 '{ClasicRo}.docx
Mircea Eliade - Comentarii la legenda mesterului Manole 1.0 '{ClasicRo}.docx
Mircea Eliade - Incognito la Buchenwald 0.9 '{ClasicRo}.docx
Mircea Eliade - In curte la Dionis 0.9 '{ClasicRo}.docx
Mircea Eliade - India 1.0 '{ClasicRo}.docx
Mircea Eliade - Introducere in tantrismul secret 0.99 '{ClasicRo}.docx
Mircea Eliade - Ivan 0.99 '{ClasicRo}.docx
Mircea Eliade - La tiganci 0.9 '{ClasicRo}.docx
Mircea Eliade - Les trois graces 0.99 '{ClasicRo}.docx
Mircea Eliade - Maitreyi 1.0 '{ClasicRo}.docx
Mircea Eliade - Noaptea de Sanziene 0.9 '{ClasicRo}.docx
Mircea Eliade - Nunta in cer 1.0 '{ClasicRo}.docx
Mircea Eliade - Oceanografie 1.0 '{ClasicRo}.docx
Mircea Eliade - Pelerina 0.99 '{ClasicRo}.docx
Mircea Eliade - Psihologia meditatiei indiene 1.0 '{ClasicRo}.docx
Mircea Eliade - Sacrul si profanul 0.7 '{ClasicRo}.docx
Mircea Eliade - Salazar 0.9 '{ClasicRo}.docx
Mircea Eliade - Santurile 0.99 '{ClasicRo}.docx
Mircea Eliade - Sarpele 0.99 '{ClasicRo}.docx
Mircea Eliade - Tinerete fara de tinerete 0.99 '{ClasicRo}.docx
Mircea Eliade - Uniforme de general 0.99 '{ClasicRo}.docx

./Mircea Geoana:
Mircea Geoana - Incredere 0.99 '{Politica}.docx

./Mircea M. Ionescu:
Mircea M. Ionescu - Asasinul acorda interviuri 1.0 '{Politista}.docx

./Mircea Malita:
Mircea Malita - Pietre vii 0.9 '{Spiritualitate}.docx

./Mircea Micu:
Mircea Micu - Intamplari cu scriitori 1.0 '{ClasicRo}.docx
Mircea Micu - Patima 1.0 '{Dragoste}.docx

./Mircea Moisescu:
Mircea Moisescu - Cosmonatul de piatra 1.0 '{SF}.docx

./Mircea Naumescu:
Mircea Naumescu - Marea experienta 0.9 '{SF}.docx

./Mircea Nedelciu:
Mircea Nedelciu - Si ieri va fi o zi 0.7 '{Literatura}.docx

./Mircea Oprita:
Mircea Oprita - Argonautica 1.0 '{SF}.docx
Mircea Oprita - Intalnire cu meduza 0.9 '{SF}.docx
Mircea Oprita - Noptile memoriei 1.0 '{SF}.docx
Mircea Oprita - Parodii 0.9 '{SF}.docx

./Mircea Popescu:
Mircea Popescu - Dosarul caprioarei aurii V1 1.0 '{ClubulTemerarilor}.docx
Mircea Popescu - Dosarul caprioarei aurii V2 1.0 '{ClubulTemerarilor}.docx
Mircea Popescu - Enigma sagetii albastre 1.0 '{ClubulTemerarilor}.docx
Mircea Popescu - Secretul Elizei Dornescu 2.0 '{Politista}.docx
Mircea Popescu - Sfaramatorul de stanci 1.0 '{Calatorii}.docx

./Mircea Popp:
Mircea Popp - Intamplare cu Agatha 0.99 '{SF}.docx

./Mircea Radina:
Mircea Radina - Milioanele lui Belami V1 1.0 '{ClubulTemerarilor}.docx
Mircea Radina - Milioanele lui Belami V2 1.0 '{ClubulTemerarilor}.docx
Mircea Radina - Paianjenul isi tese panza 2.0 '{Politista}.docx
Mircea Radina - Strada Z numarul 4 1.0 '{ClubulTemerarilor}.docx

./Mircea Santimbreanu:
Mircea Santimbreanu - Mama mamutilor mahmuri 1.0 '{BasmesiPovesti}.docx
Mircea Santimbreanu - Recreatia mare 0.8 '{BasmesiPovesti}.docx

./Mircea Serbanescu:
Mircea Serbanescu - Aventura in lumea albastra 1.0 '{ClubulTemerarilor}.docx
Mircea Serbanescu - Balada spatiala 0.9 '{SF}.docx
Mircea Serbanescu - Misterioasa sirena 1.0 '{SF}.docx
Mircea Serbanescu - Toamna cand bate vantul 1.0 '{SF}.docx
Mircea Serbanescu - Uluitoarea transmigratie 1.0 '{SF}.docx

./Mircea Vaida:
Mircea Vaida - Pe urmele lui Lucian Blaga 0.99 '{Biografie}.docx

./Mircea Vulcanescu:
Mircea Vulcanescu - Ultimul cuvant 0.9 '{Biografie}.docx

./Mireille Calmel:
Mireille Calmel - Cantul vrajitoarelor V1 2.0 '{Supranatural}.docx
Mireille Calmel - Cantul vrajitoarelor V2 1.0 '{Supranatural}.docx
Mireille Calmel - Cantul vrajitoarelor V3 1.0 '{Supranatural}.docx
Mireille Calmel - Femeia Pirat - V1 Slujitorii regelui 1.0 '{Supranatural}.docx
Mireille Calmel - Femeia Pirat - V2 Jocul umbrelor 1.0 '{Supranatural}.docx

./Mireille Carmel:
Mireille Carmel - Jocul Lupoaicelor - V1 Camera blestemata 1.0 '{Supranatural}.docx
Mireille Carmel - Jocul Lupoaicelor - V2 Razbunarea lui Isabeau 1.0 '{Supranatural}.docx

./Mireille Simoni Abbat:
Mireille Simoni Abbat - Aztecii 1.0 '{Civilizatii}.docx

./Miron Costin:
Miron Costin - De neamul moldovenilor 1.0 '{ClasicRo}.docx
Miron Costin - Letopisetul Tarii Moldovei 1.0 '{ClasicRo}.docx
Miron Costin - Viata lumii 1.0 '{ClasicRo}.docx

./Miron Manega:
Miron Manega - Moartea unui ziarist 0.9 '{Jurnalism}.docx

./Miron Scorobete:
Miron Scorobete - Comoara din pestera scheletelor V1 1.0 '{ClubulTemerarilor}.docx
Miron Scorobete - Comoara din pestera scheletelor V2 1.0 '{ClubulTemerarilor}.docx
Miron Scorobete - Crancena lupta dintre Ate si Abile 0.9 '{SF}.docx
Miron Scorobete - Femeia venita de sus 2.0 '{SF}.docx
Miron Scorobete - Meduza 0.99 '{Politista}.docx

./Misha Defonseca:
Misha Defonseca - Printre lupi 1.0 '{Tineret}.docx

./Misterele Lumii:
Misterele Lumii - Cele mai mari organizatii secrete 1.0 '{MistersiStiinta}.docx
Misterele Lumii - Crime neelucidate 1.0 '{MistersiStiinta}.docx

./Mitch Albom:
Mitch Albom - Cei 5 oameni pe care ii intalnesti in rai 1.0 '{Literatura}.docx
Mitch Albom - Inca o zi 1.0 '{Literatura}.docx
Mitch Albom - Marti cu Morrie 2.0 '{Literatura}.docx
Mitch Albom - Masura timpului 1.0 '{Literatura}.docx

./Mitos Micleusanu & Florin Braghis:
Mitos Micleusanu & Florin Braghis - Nekrotitanium 1.0 '{SF}.docx

./Miyamoto Musashi:
Miyamoto Musashi - Cartea celor cinci cercuri 1.0 '{Spiritualitate}.docx

./Mo Hayder:
Mo Hayder - Jack Caffery - V1 Omul pasare 1.0 '{Politista}.docx
Mo Hayder - Jack Caffery - V2 Tratamentul 1.0 '{Politista}.docx

./Moira Fowley Doyle:
Moira Fowley Doyle - Sezonul accidentelor 1.0 '{Literatura}.docx

./Moliere:
Moliere - Tartuffe 0.99 '{Teatru}.docx
Moliere - Teatru 1.0 '{Teatru}.docx

./Mona Gloria:
Mona Gloria - In tara uriasilor rosii 1.0 '{Tineret}.docx

./Monica Baker:
Monica Baker - Dureri disparute 0.9 '{Dragoste}.docx

./Monica Bauer:
Monica Bauer - O noapte a daruirii totale 0.99 '{Romance}.docx

./Monica Brodie:
Monica Brodie - Oglinda sufletului 0.99 '{Dragoste}.docx

./Monica Hesse:
Monica Hesse - Fata cu palton albastru 1.0 '{Dragoste}.docx

./Monica Lewty:
Monica Lewty - Capcana dorintelor 0.99 '{Dragoste}.docx
Monica Lewty - Un chirias ciudat 0.99 '{Dragoste}.docx

./Monica Sabolo:
Monica Sabolo - Sora mea, Summer 1.0 '{Literatura}.docx

./Monica Taylor:
Monica Taylor - Printesa fara palat 0.99 '{Dragoste}.docx

./Monica Vlad:
Monica Vlad - Catalin Botezatu - Trei vieti pedeapsa 0.7 '{Biografie}.docx

./Monika Peetz:
Monika Peetz - 7 zile fara 1.0 '{Literatura}.docx
Monika Peetz - Doamnele de Marti 1.0 '{Literatura}.docx
Monika Peetz - Doamnele de Marti in deruta 1.0 '{Literatura}.docx

./Monique Roffey:
Monique Roffey - Femeia alba pe bicicleta verde 0.9 '{Literatura}.docx

./Mons Kallentoft:
Mons Kallentoft - Sacrificiul din miezul iernii 1.0 '{Politista}.docx
Mons Kallentoft - Toamna 1.0 '{Politista}.docx

./Montague Rhodes James:
Montague Rhodes James - Inimi pierdute 1.0 '{Literatura}.docx

./Moony Witcher:
Moony Witcher - Geno si sigiliul negru al doamnei Crikken 1.0 '{Tineret}.docx

./Moore Clifford:
Moore Clifford - Al doilea amant al doamnei Chatterley 0.9 '{Dragoste}.docx

./Mordecai Roshwald:
Mordecai Roshwald - Ultimatum, ultimele zile ale unui razboi atomic 2.0 '{ActiuneRazboi}.docx

./Morgan Matson:
Morgan Matson - Marea calatorie a lui Amy si Roger 1.0 '{Tineret}.docx

./Morgan Rice:
Morgan Rice - Inelul Vrajitorului - V1 Calea eroilor 1.0 '{Supranatural}.docx
Morgan Rice - Inelul Vrajitorului - V2 Marsul regilor 1.0 '{Supranatural}.docx
Morgan Rice - Inelul Vrajitorului - V3 Soarta dragonilor 1.0 '{Supranatural}.docx
Morgan Rice - Inelul Vrajitorului - V4 Strigat de onoare 1.0 '{Supranatural}.docx
Morgan Rice - Inelul Vrajitorului - V5 Juramant de glorie 0.9 '{Supranatural}.docx

./Morihei Eushiba:
Morihei Eushiba - Arta pacii 0.2 '{Spiritualitate}.docx

./Mos Butmaloiu:
Mos Butmaloiu - Calauza mica 0.9 '{Diverse}.docx

./Mose Dayan:
Mose Dayan - Istoria vietii mele 1.0 '{Istorie}.docx

./Mo Yan:
Mo Yan - Baladele usturoiului din paradis 1.0 '{Literatura}.docx
Mo Yan - Broaste 1.0 '{Literatura}.docx
Mo Yan - Obosit de viata, obosit de moarte 1.0 '{Literatura}.docx
Mo Yan - Sorgul rosu 1.0 '{Literatura}.docx
Mo Yan - Tara vinului 0.99 '{Literatura}.docx

./Mugurel Cornila:
Mugurel Cornila - Scrisoare pentru mai tarziu 5.0 '{SF}.docx

./Mukesh Kapila & Damien Lewis:
Mukesh Kapila & Damien Lewis - Masacrul din Darfur 1.0 '{Jurnalism}.docx

./Mukhtar Mai:
Mukhtar Mai - Dezonorata 0.99 '{Diverse}.docx

./Mulk Anand:
Mulk Anand - Culii 1.0 '{Literatura}.docx

./Multatuli:
Multatuli - Max Havelaar in Indiile olandeze 1.0 '{Literatura}.docx

./Munkacsi Miklos:
Munkacsi Miklos - Sfidarea 1.0 '{Politista}.docx

./Muriel Barbery:
Muriel Barbery - Eleganta ariciului 0.9 '{Diverse}.docx
Muriel Barbery - Viata elfilor 1.0 '{Supranatural}.docx

./Muriel Spark:
Muriel Spark - Cei mai frumosi ani 0.99 '{Romance}.docx
Muriel Spark - Mandrie ranita 0.99 '{Dragoste}.docx

./Murray Leinster:
Murray Leinster - Gaura cheii 1.0 '{SF}.docx

./Murray N. Rothbard:
Murray N. Rothbard - Ce le-a facut statul banilor nostri 0.99 '{Economie}.docx

./Mykolas Sluckis:
Mykolas Sluckis - Spre culmi dus-intors 0.6 '{Literatura}.docx
```

